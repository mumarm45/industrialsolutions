package com.app.security



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(RestService)
class RestServiceSpec {

    void testSomething() {
        fail "Implement me"
    }
}
