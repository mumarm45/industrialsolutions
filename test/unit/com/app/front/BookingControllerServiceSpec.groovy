package com.app.front


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(BookingControllerService)
class BookingControllerServiceSpec {

    void testSomething() {
        fail "Implement me"
    }
}
