package com.app.front

import com.app.security.User
/**
 * Customer
 * A domain class describes the data object and it's mapping to the database
 */
class Customer {
    static final int OTHER = 0
    static final int END_USER = 1
    static final int DIE_MAKER = 2
    static final int MIDDLE_MAN = 3



    String name
    String position
    String companyName
    String billingAddress
    int customerType
    String reference
    String mobileNumberPrimary
    String mobileNumberSecondary
    String landLine
    String email
    String web
    String remark
    boolean status
    String deActivateReason

    User createdBy
    User updatedBy
    Date dateCreated
    Date lastUpdated
    static constraints = {

        name nullable:false,unique: true
        position nullable: true,blank:true
        companyName nullable: true,blank:true
        billingAddress nullable: true,blank:true
        customerType nullable: true,blank:true
        reference nullable: true,blank:true
        mobileNumberPrimary nullable: true,blank:true
        mobileNumberSecondary nullable: true,blank:true
        landLine nullable: true,blank:true
        email nullable: true,blank:true
        web nullable: true,blank:true
        remark nullable: true,blank:true
        status nullable: true,blank:true
        deActivateReason nullable: true,blank:true

    }
    static mapping = {


        //customerType sqlType:int

    }



    /*
     * Methods of the Domain Class
     */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
		return "${name}";
	}
}
