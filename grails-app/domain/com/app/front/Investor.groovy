package com.app.front

import com.app.security.User

/**
 * Investor
 * A domain class describes the data object and it's mapping to the database
 */
class Investor {

    String name
    String sirName
    String address
    String reference
    String mobileNumber
    String email
    String bankName
    String branchName
    String branchCode
    String bankAccount
    String bankAccountType
    int investedAmount
    int amountInPercentage
    boolean status
    String deActivateReason

    User createdBy
    User updatedBy
    Date dateCreated
    Date lastUpdated
    static mapping = {
        table('investor')
    }

    static constraints = {
        name nullable: false,unique: true
        sirName nullable:true,blank:true
        address nullable: true,blank:true
        reference nullable: true,blank:true
        mobileNumber nullable: false,blank: true
        email nullable: true,blank:true
        bankName nullable: true,blank:true
        branchName nullable: true,blank:true
        branchCode nullable: true,blank:true
        bankAccount nullable: true,blank:true
        bankAccountType nullable: true,blank:true
        deActivateReason nullable: true,blank:true

    }

    /*
     * Methods of the Domain Class
     */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
		return "${sirName}";
	}
}
