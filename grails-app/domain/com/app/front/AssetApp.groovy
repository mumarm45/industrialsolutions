package com.app.front


import com.app.security.User
/**
 * AssetApp
 * A domain class describes the data object and it's mapping to the database
 */
class AssetApp {
     String name
     String make
     String model
     int serialNumber
     int size
     boolean warranty
     String purchaseDate
     String expiryDate
     String warrantyDate

    Date dateCreated
    Date lastUpdated
    User createdBy
    User updatedBy

    static constraints = {
        name nullable: false,blank: false,unique: true
        make nullable: true,blank: true
        model nullable: true,blank: true
       // serialNumber nullable: true,blank: true
//        size nullable: true,blank: true
        purchaseDate nullable: true,blank: true
        expiryDate nullable: true,blank: true
        warrantyDate nullable: true,blank: true
    }

    static mapping = {

      expiryDate sqlType: 'datetime'
        purchaseDate sqlType: 'datetime'
        warrantyDate sqlType: 'datetime'
    }

    /*
     * Methods of the Domain Class
     */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
		return "${name}";
	}
}
