package com.app.front

import com.app.security.User

/**
 * WorkOrder
 * A domain class describes the data object and it's mapping to the database
 */
class WorkOrder {
    String bookingDate
    String customerName

    Customer customer
    String committedDate
    Date dateCreated
    Date lastUpdated
    User createdBy
    User updatedBy
    static hasMany = [jobs:Jobs]

    static mapping = {
        bookingDate sqlType: 'datetime'
        committedDate sqlType: 'datetime'
    }

    static constraints = {
        bookingDate nullable: false , blank: false
        customerName nullable: false , blank:false
        committedDate nullable: false, blank: false
        customer nullable: false, blank: false
    }

    /*
     * Methods of the Domain Class
     */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
		StringBuilder booking = new StringBuilder()
        def comma = ' , '
        booking.append("Booking Date:").append(bookingDate).append(comma)
        booking.append("Committed Date:").append(committedDate).append(comma)
        booking.append("Customer Name:").append(customerName).append(comma)

        booking.append("Customer").append(customer).append(comma)
        return booking.toString()
	}
}
