package com.app.front

import com.app.security.User

/**
 * Employee
 * A domain class describes the data object and it's mapping to the database
 */
class Employee {
     String name
    String fullName
    String sirName
    Date dateOfBirth
    String address
    String idCard
    String mobileNumberPrimary
    String mobileNumberSecondary
    String reference
    String designation
    String salary
    boolean status
    String deactivateReason

    User createdBy
    User updatedBy
    Date dateCreated
    Date lastUpdated
    static constraints = {
        name nullable: false,blank: false,unique: true
        address nullable:true,blank:true
        fullName nullable:true,blank:true
        sirName nullable:true,blank:true
        salary nullable:true,blank:true
        dateOfBirth nullable:true,blank:true
        idCard nullable:true,blank:true
        mobileNumberPrimary nullable:true
        mobileNumberSecondary nullable:true
        reference nullable:true,blank:true
        designation nullable:true,blank:true
        deactivateReason nullable:true,blank:true
    }

    static mapping = {
    }

    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
