package com.app.front

import com.app.security.User
import net.sourceforge.jtds.jdbc.DateTime

/**
 * Jobs
 * A domain class describes the data object and it's mapping to the database
 */
class Jobs {
    String jobNumber
    String jobTitle
    String machineFileName
    Employee programedBy
    Employee settingsBy
    Employee checkedBy
    Employee assignTo
    String committedDate
    String approxTime
    String startTime
    String endTime
    String markingOnJob
    String mirror
    String clearance
    String offSet
    String programSize
    String originalSize
    String saleAccountName
    String saleAccountNo
    int wcLength
    int wcHeight
    int wcQuantityHours
    int wcRate
    int wcTotal
    int currentRatio
    int priority
    String customerComments
    String operatorComments

    Date dateCreated
    Date lastUpdated
    User createdBy
    User updatedBy

	static belongsTo	= [WorkOrder]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.

    static mapping = {
         committedDate sqlType: 'datetime'


    }

    static constraints = {
        jobTitle nullable: false,   blank: true
        machineFileName nullable: true, blank: true
        programedBy nullable: true, blank: true
        settingsBy nullable: true, blank: true
        checkedBy nullable: true, blank: true
        assignTo nullable: true, blank: true
        committedDate nullable: true, blank: true
        approxTime nullable: true, blank: true
        startTime nullable: true, blank: true
        endTime nullable: true, blank: true
        markingOnJob nullable: true, blank: true
        mirror nullable: true, blank: true
        clearance nullable: true, blank: true
        offSet nullable: true, blank: true
        programSize nullable: true, blank: true
        originalSize nullable: true, blank: true
        saleAccountName nullable: true, blank: true
        saleAccountNo nullable: true, blank: true
        wcLength nullable: true, blank: true
        wcHeight nullable: true, blank: true
        wcRate nullable: true, blank: true
        wcQuantityHours nullable: true, blank: true
        wcTotal nullable: true, blank: true
        currentRatio nullable: true, blank: true
        priority nullable: true, blank: true
        customerComments nullable: true, blank: true
        operatorComments nullable: true, blank: true

    }

    /*
     * Methods of the Domain Class
     */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
        StringBuilder job = new StringBuilder()
        def comma = ' , '

        job.append("Job Number:").append(jobNumber).append(comma)
        job.append("Job Title:").append(jobTitle).append(comma)
        job.append("Programed By:").append(programedBy).append(comma)
        job.append("Assigned To:").append(assignTo).append(comma)
        job.append("Committed Date:").append(committedDate).append(comma)
        job.append("Priority: ").append(priority).append(comma)
        return job.toString()
	}
}
