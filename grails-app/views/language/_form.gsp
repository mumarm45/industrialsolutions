<%@ page import="com.app.translate.Language" %>



<div class="fieldcontain ${hasErrors(bean: languageInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="language.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${languageInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: languageInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="language.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${languageInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: languageInstance, field: 'languageCode', 'error')} required">
	<label for="languageCode">
		<g:message code="language.languageCode.label" default="Language Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="languageCode" required="" value="${languageInstance?.languageCode}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: languageInstance, field: 'updatedBy', 'error')} ">
	<label for="updatedBy">
		<g:message code="language.updatedBy.label" default="Updated By" />
		
	</label>
	<g:select id="updatedBy" name="updatedBy.id" from="${com.app.security.User.list()}" optionKey="id" value="${languageInstance?.updatedBy?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: languageInstance, field: 'createdBy', 'error')} ">
	<label for="createdBy">
		<g:message code="language.createdBy.label" default="Created By" />
		
	</label>
	<g:select id="createdBy" name="createdBy.id" from="${com.app.security.User.list()}" optionKey="id" value="${languageInstance?.createdBy?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

