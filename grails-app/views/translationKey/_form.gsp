<%@ page import="com.app.ef.translate.TranslationKey" %>



<div class="fieldcontain ${hasErrors(bean: translationKeyInstance, field: 'key_id', 'error')} required">
	<label for="key_id">
		<g:message code="translationKey.key_id.label" default="Keyid" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="key_id" required="" value="${translationKeyInstance?.key_id}"/>

</div>



