<!DOCTYPE html>
<html lang="en" ng-app="indapp">
<head>
    <meta charset="UTF-8"/>
    <title>${grailsApplication.metadata['app.name']}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'/>
    <!-- bootstrap 3.0.2 -->
    <asset:stylesheet href="bootstrap.min.css"/>
    <!-- font Awesome -->
    <asset:stylesheet href="font-awesome.min.css"/>
    <!-- Ionicons -->
    <asset:stylesheet href="ionicons.min.css"/>
    <!-- Morris chart -->
    <asset:stylesheet href="morris/morris.css"/>
    <!-- jvectormap -->
    <asset:stylesheet href="jvectormap/jquery-jvectormap-1.2.2.css"/>
    <!-- fullCalendar -->
    <asset:stylesheet href="fullcalendar/fullcalendar.css"/>
    <!-- Daterange picker -->
    <asset:stylesheet href="daterangepicker/daterangepicker-bs3.css"/>
    <!-- bootstrap wysihtml5 - text editor -->
    <asset:stylesheet href="bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"/>
    <!-- Theme style -->
    <asset:stylesheet href="AdminLTE.css"/>
    <!-- NG TABLES -->
    <asset:stylesheet href="plugins/ngtable/ng-table.min.css"/>
    %{--<link href="css/application.css" rel="stylesheet" type="text/css" />--}%
    <asset:stylesheet href="application.css"/>
    <!--X editable -->
    <asset:stylesheet href="plugins/angular-xeditable/css/xeditable.css"/>
    <!-- toaster-->
    <asset:stylesheet href="toaster.css"/>
    <asset:stylesheet href="bootstrap-switch.min.css"/>

    <asset:stylesheet href="/ng-table/ng-table.css"/>
    <asset:stylesheet href="ng-table.css"/>


    %{--Angualr Bootstrap DateTime Picker--}%
    <asset:stylesheet href="datetimepicker.css"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <asset:javascript src="html5shiv.js"/>

    <asset:javascript src="respond.min.js"/>

    <!--[endif]-->
</head>
<body class="skin-blue" ng-controller="ApplicationController">
<toaster-container toaster-options="{'limit': 0,'tap-to-dismiss': true,
'position-class': 'toast-bottom-left',
'close-button':true,
'newest-on-top': true,
'show-duration': '100',
'hide-duration': '1000',
'time-out': '5000',
'extended-time-out': '1000',
'show-easing': 'swing',
'hide-easing': 'linear',
'show-method': 'fadeIn',
'hide-method': 'fadeOut'
}"></toaster-container>
<g:render template="/templates/topMenu"/>

<div class="wrapper row-offcanvas row-offcanvas-left">
    <g:render template="/templates/sideMenu"/>
    <g:layoutBody/>

</div>

<!-- jQuery 2.0.2 -->
<asset:javascript src="jquery-2.1.1.min.js"/>
<!-- jQuery UI 1.10.3 -->
<asset:javascript src="jquery-ui-1.10.3.min.js"/>
<!-- Morris.js charts -->
<asset:javascript src="raphael-min.js"/>
<asset:javascript src="plugins/morris/morris.min.js"/>


<!-- Bootstrap -->
<asset:javascript src="bootstrap.min.js"/>
<asset:javascript src="lodash.js"/>
<asset:javascript src="moment.js"/>

<!-- Sparkline -->
<asset:javascript src="plugins/sparkline/jquery.sparkline.min.js"/>
<!-- jvectormap -->
<asset:javascript src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"/>
<asset:javascript src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"/>
<!-- fullCalendar -->
<asset:javascript src="plugins/fullcalendar/fullcalendar.min.js"/>
<!-- jQuery Knob Chart -->
<asset:javascript src="plugins/jqueryKnob/jquery.knob.js"/>
<!-- daterangepicker -->
<asset:javascript src="plugins/daterangepicker/daterangepicker.js"/>
<!-- Bootstrap WYSIHTML5 -->
<asset:javascript src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"/>
<!-- iCheck -->
<asset:javascript src="plugins/iCheck/icheck.min.js"/>

<!-- AdminLTE App -->
<asset:javascript src="AdminLTE/app.js"/>
<!-- Angular -->
<asset:javascript src="angular.js"/>
<asset:javascript src="angular-ui.js"/>
<asset:javascript src="angular-animate.min.js"/>
<asset:javascript src="angular-resource.min.js"/>
<asset:javascript src="angular-resource.js"/>
<asset:javascript src="angular-route.min.js"/>
<asset:javascript src="plugins/ngtable/ng-table.js"/>
<asset:javascript src="toaster.js"/>
<asset:javascript src="bootstrap-switch.min.js"/>
<asset:javascript src="custom-form-elements.js"/>
<asset:javascript src="ngStorage.min.js"/>



<!--X editable -->
<asset:javascript src="plugins/angular-xeditable/js/xeditable.js"/>
<asset:javascript src="bootstrap-editable.js"/>
<asset:javascript src="bootstrap-hover.js"/>
<asset:javascript src="ui-bootstrap.min.js"/>
<asset:javascript src="angular-translate.min.js"/>
<asset:javascript src="angular-translate-loadUrl.min.js"/>
<asset:javascript src="angular-cookies.min.js"/>
<asset:javascript src="angular-translate-storage-cookies.min.js"/>
<asset:javascript src="angular-moment.min.js"/>
<asset:javascript src="angular-bootstrap-switch.min.js"/>



<!-- Angular App -->
<asset:javascript src="angular/app.js"/>

%{--toaster--}%
<asset:javascript src="angular/toaster/toaster.min.js"/>
<asset:stylesheet href="toaster.css"/>
%{--chosen--}%
<asset:stylesheet href="chosen.css"/>
<asset:javascript src="chosen.jquery.min.js"/>

%{--datetime--}%
<asset:stylesheet href="bootstrap.datetimepicker.min.css"/>
<asset:javascript src="bootstrap.datetimepicker.min.js"/>

<!-- Interceptor-->
<asset:javascript src="angular/controllers/auth/AuthInterceptor.js"/>
<!-- Auth -->
<asset:javascript src="angular/controllers/auth/PermissionService.js"/>
<asset:javascript src="angular/controllers/auth/AuthService.js"/>
<asset:javascript src="angular/controllers/auth/Session.js"/>

<asset:javascript src="angular/controllers/mainController.js"/>

<asset:javascript src="/angular/controllers/evr/callsRecorded.js"/>
<asset:javascript src="/angular/controllers/evr/callsArchived.js"/>
<asset:javascript src="/angular/controllers/evr/showCallDetail.js"/>
<asset:javascript src="/angular/controllers/evr/assignTag.js"/>
<!-- Directivs -->
<asset:javascript src="angular/directives/strip.js"/>
<asset:javascript src="angular/directives/ng-enter.js"/>
<asset:javascript src="angular/directives/ex-editable.js"/>
<asset:javascript src="angular/directives/sideMenuAuthentication.js"/>

<!-- Services-->
<asset:javascript src="angular/services/userResourceService.js"/>
<asset:javascript src="angular/services/investorResourceService.js"/>
<asset:javascript src="angular/services/employeeResourceService.js"/>
<asset:javascript src="angular/services/customerResourceService.js"/>
<asset:javascript src="angular/services/assetAppResourceService.js"/>
<asset:javascript src="angular/services/bookingResourceService.js"/>
<asset:javascript src="angular/services/workOrderResourceService.js"/>
<asset:javascript src="angular/services/jobsResourceService.js"/>
<asset:javascript src="angular/services/roleResourceService.js"/>
<asset:javascript src="angular/services/permissionResourceService.js"/>
<asset:javascript src="angular/services/utils.js"/>

<asset:javascript src="angular/services/evrService.js"/>
<asset:javascript src="angular/controllers/confirm_dialog_ctrl.js"/>
<!-- User-->
<asset:javascript src="angular/controllers/user/detailController.js"/>
<asset:javascript src="angular/controllers/user/listController.js"/>

<!-- investor-->
<asset:javascript src="angular/controllers/investor/detailController.js"/>
<asset:javascript src="angular/controllers/investor/listController.js"/>
<asset:javascript src="angular/controllers/investor/addController.js"/>
<!-- customer-->
<asset:javascript src="angular/controllers/customer/detailController.js"/>
<asset:javascript src="angular/controllers/customer/listController.js"/>
<asset:javascript src="angular/controllers/customer/addController.js"/>

<!-- assetApp-->
<asset:javascript src="angular/controllers/assetApp/detailController.js"/>
<asset:javascript src="angular/controllers/assetApp/listController.js"/>
<asset:javascript src="angular/controllers/assetApp/addController.js"/>
<!-- workOrder-->
<asset:javascript src="angular/controllers/workOrder/detailController.js"/>
<asset:javascript src="angular/controllers/workOrder/listController.js"/>
<asset:javascript src="angular/controllers/workOrder/addController.js"/>

<!-- jobs-->
<asset:javascript src="angular/controllers/jobs/detailController.js"/>
<asset:javascript src="angular/controllers/jobs/listController.js"/>
<asset:javascript src="angular/controllers/jobs/addController.js"/>
<!-- booking-->
<asset:javascript src="angular/controllers/booking/detailController.js"/>
<asset:javascript src="angular/controllers/booking/listController.js"/>
<asset:javascript src="angular/controllers/booking/addController.js"/>
<!-- employee-->
<asset:javascript src="angular/controllers/employee/detailController.js"/>
<asset:javascript src="angular/controllers/employee/listController.js"/>
<asset:javascript src="angular/controllers/employee/addController.js"/>
<!-- Permission-->
<asset:javascript src="angular/controllers/permission/listController.js"/>
<!-- Role-->
<asset:javascript src="angular/controllers/role/listController.js"/>
<asset:javascript src="angular/controllers/role/createController.js"/>
<asset:javascript src="angular/controllers/role/detailController.js"/>
<!-- Login -->
<asset:javascript src="angular/controllers/loginController.js"/>

<asset:javascript src="angular/controllers/user/addController.js"/>
<asset:javascript src="angular/controllers/controllers.js"/>

%{-- Angualr Bootstrap DateTime Picker--}%

<asset:javascript src="datetimepicker.js"/>
%{--Angualr Bootstrap Date Range Picker--}%
<asset:javascript src="angular-daterangepicker.js"/>
<script>
    var appBaseUrl = '/'+'${grailsApplication.metadata['app.name']}';



</script>

</body>
</html>