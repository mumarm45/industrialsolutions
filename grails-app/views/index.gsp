
<html lang="en" ng-app="indapp">
<head>
    <meta charset="UTF-8"/>
    <meta name="layout" content="main" />
    <title>${grailsApplication.metadata['app.name']}</title>

</head>
<body>
<aside class="right-side">

   <div>
    <!-- Main content -->
    <section class="content">
    <div ng-view></div>
    </section><!-- /.content -->
   </div>

</aside>

</body>
</html>