import com.app.security.Permission
import com.app.security.User
import com.app.translate.Language
import com.app.translate.Translation
import com.app.translate.TranslationKey
import org.apache.shiro.crypto.hash.Sha256Hash

class BootStrap {

    def init = { servletContext ->

        if (!User.findByUsername("admin")) {
            println "Reach in bootstrap class if"
            log.info "-----------------New values entering into table ------------------------"

            // User related Permissions
            new Permission( expression: 'user:*', name: 'com.ef.pcs.bootstrap.all.user.name',
                    description: 'com.ef.pcs.bootstrap.all.user').save()
            new Permission( expression: 'user:show', name: 'com.ef.pcs.bootstrap.show.user.name',
                    description: 'com.ef.pcs.bootstrap.show.user').save()
            new Permission( expression: 'user:save,usernameExist,emailExist,save', name: 'com.ef.pcs.bootstrap.create.user.name',
                    description: 'com.ef.pcs.bootstrap.create.user').save()
            new Permission( expression: 'user:edit,update', name: 'com.ef.pcs.bootstrap.edit.user.name',
                    description: 'com.ef.pcs.bootstrap.edit.user').save()
            new Permission( expression: 'user:index', name: 'com.ef.pcs.bootstrap.list.user.name',
                    description: 'com.ef.pcs.bootstrap.list.user').save()
            new Permission( expression: 'user:delete,deleteSelected', name: 'com.ef.pcs.bootstrap.delete.user.name',
                    description: 'com.ef.pcs.bootstrap.delete.user').save()
            new Permission( expression: 'user:activateOrBlockUser', name: 'com.ef.pcs.bootstrap.activate.user.name',
                    description: 'com.ef.pcs.bootstrap.activate.user').save()
             new Permission( expression: 'user:viewRoles', name: 'com.ef.pcs.bootstrap.role.user.name',
                    description: 'com.ef.pcs.bootstrap.role.user').save()
            new Permission( expression: 'user:assignAndRevokeRole', name: 'com.ef.pcs.bootstrap.assignRole.user.name',
                    description: 'com.ef.pcs.bootstrap.assignRole.user').save()
             new Permission( expression: 'user:viewPermissions', name: 'com.ef.pcs.bootstrap.viewPermission.user.name',
                    description: 'com.ef.pcs.bootstrap.viewPermission.user').save()
            new Permission( expression: 'user:assignAndRevokePermission', name: 'com.ef.pcs.bootstrap.assignPermission.user.name',
                    description: 'com.ef.pcs.bootstrap.assignPermission.user').save()
            new Permission( expression: 'user:changePassword', name: 'com.ef.pcs.bootstrap.changePassword.user.name',
                    description: 'com.ef.pcs.bootstrap.resetPassword.user').save()

            // Role related permissions
            new Permission( expression: 'role:*', name: 'com.ef.pcs.bootstrap.all.role.name',
                    description: 'com.ef.pcs.bootstrap.all.role').save()
            new Permission( expression: 'role:show', name: 'com.ef.pcs.bootstrap.show.role.name',
                    description: 'com.ef.pcs.bootstrap.show.role').save()
            new Permission( expression: 'role:save,nameExist', name: 'com.ef.pcs.bootstrap.create.role.name',
                    description: 'Can create new Role').save()
            new Permission( expression: 'role:edit,update', name: 'com.ef.pcs.bootstrap.edit.role.name',
                    description: 'com.ef.pcs.bootstrap.edit.role').save()
            new Permission( expression: 'role:index', name: 'com.ef.pcs.bootstrap.list.role.name',
                    description: 'com.ef.pcs.bootstrap.list.role').save()
            new Permission( expression: 'role:delete,deleteSelected', name: 'com.ef.pcs.bootstrap.delete.role.name',
                    description: 'com.ef.pcs.bootstrap.delete.role').save()
            new Permission( expression: 'role:viewUsers', name: 'com.ef.pcs.bootstrap.viewMember.role.name',
                    description: 'com.ef.pcs.bootstrap.viewMember.role').save()
            new Permission( expression: 'role:viewPermissions', name: 'com.ef.pcs.bootstrap.viewPermission.role.name',
                    description: 'com.ef.pcs.bootstrap.viewPermission.role').save()
            new Permission( expression: 'role:assignAndRevokePermission', name: 'com.ef.pcs.bootstrap.assignPermission.role.name',
                    description: 'com.ef.pcs.bootstrap.assignPermission.role').save()
            new Permission( expression: 'role:assignAndRevokeUser', name: 'com.ef.pcs.bootstrap.addMember.role.name',
                    description: 'com.ef.pcs.bootstrap.addMember.role').save()
            // Default Administrator User

            //Permission
            new Permission( expression: 'permission:index', name: 'com.ef.pcs.bootstrap.permission.list.name',
                    description: 'com.ef.pcs.bootstrap.permissions.list').save()

            def admin

            admin = new User(username: "admin",
                    password: new Sha256Hash("admiN123!").toHex(),
                    isActive: true
            )
            admin.addToPermissions("*:*")
            admin.save()

        }
        }
    def destroy = {
    }
}
