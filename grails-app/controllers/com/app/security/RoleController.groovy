package com.app.security

import grails.converters.JSON
import groovy.json.JsonSlurper

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RoleController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE",show:"GET"]

    def index(Integer max) {
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def roleCount = 0
        def query = "From Role r  "
        if(jsonFiltering.name)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND r.name LIKE '%${jsonFiltering.name}%'"
            else
                query += " WHERE r.name LIKE '%${jsonFiltering.name}%'"

        }
        if(jsonFiltering.description)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND r.description LIKE '%${jsonFiltering.description}%'"
            else
                query += " WHERE r.description LIKE '%${jsonFiltering.description}%'"

        }

        roleCount =  Role.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.name)
            query += " ORDER BY r.name " + jsonSorting.name
        else if(jsonSorting.description)
            query += " ORDER BY r.description " + jsonSorting.description

        def roleListFiltered =  Role.findAll(query,[ max:params.count as int ,
                offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("rolesList", roleListFiltered)
        def nameFiltered = false

        resultList.put("rolesCount",roleCount)

        render resultList as JSON

    }
    def show(Role roleInstance) {
        if(roleInstance ==  null){
            render status: NOT_FOUND
            return
        }

        respond roleInstance
    }
    def nameExist(){
        String email = params.name
        if(email){
            def role = Role.findByName(email)
            if(role)
                render status:FOUND
            else
                render status:ACCEPTED
        }
        else
            render status:NOT_ACCEPTABLE

    }

    @Transactional
    def save(Role roleInstance) {
        if (roleInstance == null) {
            render status: NOT_FOUND
            return
        }

        roleInstance.validate()
        if (roleInstance.hasErrors()) {
            respond roleInstance,  [status: NOT_ACCEPTABLE]
            return
        }

        roleInstance.save flush: true
        respond roleInstance, [status: CREATED]
    }

    @Transactional
    def update(Role roleInstance) {
        if (roleInstance == null) {
            render status: NOT_FOUND
            return
        }

        roleInstance.validate()
        if (roleInstance.hasErrors()) {
            respond roleInstance, [status: NOT_ACCEPTABLE]
            return
        }

        roleInstance.save flush: true
        respond roleInstance, [status: OK]
    }

    @Transactional
    def delete(Role roleInstance) {
        try{
            if (roleInstance == null) {
                render status: NOT_FOUND
                return
            }
            User[] userRoles = roleInstance.members
            if(userRoles.size() > 0){
                for(user in userRoles){
                    roleInstance.removeFromMembers(user)
                }
            }

            roleInstance.delete flush: true
            render status: NO_CONTENT

        }catch(Exception ex){
            log.error("Error in deletion, Role and error is: "+ ex )
           render status: NOT_ACCEPTABLE

        }

    }

    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def role = Role.get(id)
                if(role){
                    User[] userRoles = role.members
                    for(user in userRoles){
                        role.removeFromMembers(user)
                    }
                       role.delete(flush:true)
                        deletedContact++

                }

            }catch(Exception ex){
                log.error "Role Not Deleted and Error is :" + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedRole",deletedContact)
        responseJson.put("notDeletedRole",notDeletedContact)
        respond responseJson
    }

    def viewPermissions(){

        def query = "From Permission p"
        def resultSet = [:]
        ArrayList<Permission> rolePermissionList = new ArrayList<Permission>()
        def roleInstance = Role.findById(params.id)
        roleInstance?.permissions?.each {
            rolePermissionList.add(Permission.findByExpression(it))
        }
        int permissionListTotal  = Permission.executeQuery("Select count(*)"+query)[0]
        def permissionList = Permission.findAll(query,[ max:params.count as int ,
                offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
         resultSet = [
                permissionList: permissionList,
                rolePermissionList: rolePermissionList,
                roleInstance:roleInstance,
                permissionListTotal: permissionListTotal,
                rolePermissionTotal: rolePermissionList.size(),
        ]

        render resultSet as JSON
    }

    def viewUsers(){
        def query = "From User u"
        def resultSet = [:]
        ArrayList<User> roleUserList = new ArrayList<User>()
        def roleInstance = Role.findById(params.id)
        roleUserList = roleInstance?.members
        int userListTotal  = User.executeQuery("Select count(*)"+query)[0]
        def userList = User.findAll(query,[ max:params.count as int ,
                offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultSet = [
                userList: userList,
                roleUserList: roleUserList,
                roleInstance:roleInstance,
                userListTotal: userListTotal,
                roleUserListTotal: roleUserList.size(),
        ]

        render resultSet as JSON
    }
    @Transactional
    def assignAndRevokePermission (){
        def permission = Permission.findById(params.permissionId)
        def role = Role.findById(params.roleId)
        def checked =  params.boolean("checked")
        if(permission && role){
            if(checked)
                role.addToPermissions(permission.expression)
            else
                role.removeFromPermissions(permission.expression)


            render status: OK
        }  else
        {
            render status: NOT_FOUND
            return
        }
    }
    @Transactional
    def assignAndRevokeUser(){
        def user = User.findById(params.userId)
        def role = Role.findById(params.roleId)
        def checked =  params.boolean("checked")

        if(user && role){
            if(checked)
                role.addToMembers(user)
            else
                role.removeFromMembers(user)
            render status: OK
        }  else
        {
            render status: NOT_FOUND
            return
        }
    }
}
