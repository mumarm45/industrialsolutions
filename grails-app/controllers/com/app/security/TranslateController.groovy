package com.app.security
import com.app.translate.Language
import com.app.translate.Translation
import grails.converters.JSON

class TranslateController {
    def messageSource
    static defaultAction = "index"

    def index() {
        def languageId = params.lang
       def translationValues = [:]
       def translationText = Translation.findAllByLanguage(Language.findByLanguageCode(languageId))
       for(trans in translationText){
          translationValues.put(trans.translationKey,trans.text)

       }

       render translationValues as JSON
    }


}
