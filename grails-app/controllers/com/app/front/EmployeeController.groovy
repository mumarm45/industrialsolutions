package com.app.front

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EmployeeController {
    def employeeControllerService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        render employeeControllerService.getList(params) as JSON
    }
    def nameExist(){
        String name = params.name
        if(name){
            def role = Employee.findByName(name)
            if(role)
                render status:FOUND
            else
                render status:ACCEPTED
        }
        else
            render status:NOT_ACCEPTABLE

    }
    def show(Employee employeeInstance){
        if (employeeInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond employeeInstance
    }
    @Transactional
    def save(Employee employeeInstance) {
        if (employeeInstance == null) {
            render status: NOT_FOUND
            return
        }

        employeeInstance.validate()
        if (employeeInstance.hasErrors()) {
            respond employeeInstance, [status: NOT_ACCEPTABLE]
            return
        }

        employeeInstance.save flush: true
        respond employeeInstance, [status: CREATED]
    }

    @Transactional
    def update(Employee employeeInstance) {
        if (employeeInstance == null) {
            render status: NOT_FOUND
            return
        }

        employeeInstance.validate()
        if (employeeInstance.hasErrors()) {
            respond employeeInstance, status: NOT_ACCEPTABLE
            return
        }

        employeeInstance.save flush: true
        respond employeeInstance, [status: OK]
    }

    @Transactional
    def delete(Employee employeeInstance) {

        if (employeeInstance == null) {
            render status: NOT_FOUND
            return
        }

        employeeInstance.delete flush: true
        render status: NO_CONTENT
    }

    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def employee = Employee.get(id)
                if(employee){
                    employee.delete()
                    deletedContact++
                }

            }catch(Exception ex){
                log.error "Customer Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedEmployee",deletedContact)
        responseJson.put("notDeleteEmployee",notDeletedContact)
        respond responseJson
    }
}
