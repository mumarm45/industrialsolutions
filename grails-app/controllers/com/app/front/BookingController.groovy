package com.app.front


import grails.converters.JSON

import java.awt.print.Book


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BookingController {
      def bookingControllerService


    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        render bookingControllerService.getList(params) as JSON
    }

    @Transactional
    def save(Booking bookingInstance) {
        if (bookingInstance == null) {
            render status: NOT_FOUND
            return
        }

        bookingInstance.validate()
        if (bookingInstance.hasErrors()) {

            respond bookingInstance, status: NOT_ACCEPTABLE
            return
        }

        bookingInstance.save flush: true
        respond bookingInstance, [status: CREATED]
    }

    @Transactional
    def update(Booking bookingInstance) {
        if (bookingInstance == null) {
            render status: NOT_FOUND
            return
        }

        bookingInstance.validate()
        if (bookingInstance.hasErrors()) {
            respond bookingInstance, status: NOT_ACCEPTABLE

            return
        }

        bookingInstance.save flush: true
        respond bookingInstance, [status: OK]
    }

    @Transactional
    def delete(Booking bookingInstance) {

        if (bookingInstance == null) {
            render status: NOT_FOUND
            return
        }

        bookingInstance.delete flush: true
        render status: NO_CONTENT
    }


    def findCustomer(){
        def name = params.name

        def customer = Customer.findByName(name)
        if(!customer){
            render status: NOT_ACCEPTABLE
            return
        }


        respond customer , status:OK
    }

    def getCustomerList(){
        def customers = [:]

        def customerList = Customer.getAll()


        render customerList as JSON
    }
    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def booking = Booking.get(id)
                if(booking){
                    booking.delete()
                    deletedContact++
                }

            }catch(Exception ex){
                log.error "Booking Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedBooking",deletedContact)
        responseJson.put("notDeleteBooking",notDeletedContact)
        respond responseJson
    }

    def show(Booking bookingInstance){
        if (bookingInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond bookingInstance
    }

}
