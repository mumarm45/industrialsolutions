package com.app.front

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CustomerController {
def customerControllerService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        render customerControllerService.getList(params) as JSON
    }
    def nameExist(){
        String name = params.name
        if(name){
            def role = Customer.findByName(name)
            if(role)
                render status:FOUND
            else
                render status:ACCEPTED
        }
        else
            render status:NOT_ACCEPTABLE

    }
    def show(Customer customerInstance){
        if (customerInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond customerInstance
    }
    @Transactional
    def save(Customer customerInstance) {
        if (customerInstance == null) {
            render status: NOT_FOUND
            return
        }

        customerInstance.validate()
        if (customerInstance.hasErrors()) {
            respond customerInstance ,[status: NOT_ACCEPTABLE]
            return
        }

        customerInstance.save flush: true
        respond customerInstance, [status: CREATED]
    }

    @Transactional
    def update(Customer customerInstance) {
        if (customerInstance == null) {
            render status: NOT_FOUND
            return
        }

        customerInstance.validate()
        if (customerInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        customerInstance.save flush: true
        respond customerInstance, [status: OK]
    }

    @Transactional
    def delete(Customer customerInstance) {

        if (customerInstance == null) {
            render status: NOT_FOUND
            return
        }

        customerInstance.delete flush: true
        render status: NO_CONTENT
    }

    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def customer = Customer.get(id)
                if(customer){
                    customer.delete()
                    deletedContact++
                }

            }catch(Exception ex){
                log.error "Customer Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedCustomer",deletedContact)
        responseJson.put("notDeleteCustomer",notDeletedContact)
        respond responseJson
    }
}
