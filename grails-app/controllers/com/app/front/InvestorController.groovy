package com.app.front

import com.app.security.Role
import com.app.security.User
import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvestorController {
    def investorControllerService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
      def lists = investorControllerService.getList(params)
        render lists as JSON

    }
    def nameExist(){
        String name = params.name
        if(name){
            def role = Investor.findByName(name)
            if(role)
                render status:FOUND
            else
                render status:ACCEPTED
        }
        else
            render status:NOT_ACCEPTABLE

    }

    @Transactional
    def save(Investor investorInstance) {
        if (investorInstance == null) {
            render status: NOT_FOUND
            return
        }

        investorInstance.validate()
        if (investorInstance.hasErrors()) {
            respond investorInstance, [status: NOT_ACCEPTABLE]
            return
        }

        investorInstance.save flush:true
        respond investorInstance, [status: CREATED]
    }

    def show(Investor investorInstance){
        if (investorInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond investorInstance
    }
    @Transactional
    def update(Investor investorInstance) {
        if (investorInstance == null) {
            render status: NOT_FOUND
            return
        }

        investorInstance.validate()
        if (investorInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        investorInstance.save flush:true
        respond investorInstance, [status: OK]
    }

    @Transactional
    def delete(Investor investorInstance) {

        if (investorInstance == null) {
            render status: NOT_FOUND
            return
        }

        investorInstance.delete flush:true
        render status: NO_CONTENT
    }
    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def investor = Investor.get(id)
                if(investor){
                    investor.delete()
                        deletedContact++
                }

            }catch(Exception ex){
                log.error "Investor Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedInvestor",deletedContact)
        responseJson.put("notDeleteInvestor",notDeletedContact)
        respond responseJson
    }
}
