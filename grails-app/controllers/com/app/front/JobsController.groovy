package com.app.front

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class JobsController {
def jobsControllerService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
      render  jobsControllerService.getList(params) as JSON
    }

    @Transactional
    def save(Jobs jobsInstance) {
        if (jobsInstance == null) {
            render status: NOT_FOUND
            return
        }

        jobsInstance.validate()
        if (jobsInstance.hasErrors()) {
            respond jobsInstance,status: NOT_ACCEPTABLE
            return
        }

        jobsInstance.save flush: true
        respond jobsInstance, [status: CREATED]
    }

    @Transactional
    def update(Jobs jobsInstance) {
        if (jobsInstance == null) {
            render status: NOT_FOUND
            return
        }

        jobsInstance.validate()
        if (jobsInstance.hasErrors()) {
            respond jobsInstance, status: NOT_ACCEPTABLE
            return
        }

        jobsInstance.save flush: true
        respond jobsInstance, [status: OK]
    }

    @Transactional
    def delete(Jobs jobsInstance) {

        if (jobsInstance == null) {
            render status: NOT_FOUND
            return
        }

        jobsInstance.delete flush: true
        render status: NO_CONTENT
    }


    def findEmployee(){
        def name = params.name

        def customer = Customer.findByName(name)
        if(!customer){
            render status: NOT_ACCEPTABLE
            return
        }


        respond customer , status:OK
    }

    def getEmployeeList(){
        def employeeList = Employee.getAll()


        render employeeList as JSON
    }
    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def job = Jobs.get(id)
                if(job){
                    job.delete()
                    deletedContact++
                }

            }catch(Exception ex){
                log.error "Jobs Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedJob",deletedContact)
        responseJson.put("notDeleteJob",notDeletedContact)
        respond responseJson
    }

    def show(Jobs jobsInstance){
        if (jobsInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond jobsInstance
    }
}
