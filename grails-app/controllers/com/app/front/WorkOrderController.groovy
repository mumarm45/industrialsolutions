package com.app.front

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class WorkOrderController {
    def workOrderService
    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
       render  workOrderService.getList(params) as JSON
    }

    @Transactional
    def save(WorkOrder workOrderInstance) {
        if (workOrderInstance == null) {
            render status: NOT_FOUND
            return
        }

        workOrderInstance.validate()
        if (workOrderInstance.hasErrors()) {
            respond workOrderInstance,status: NOT_ACCEPTABLE
            return
        }

        workOrderInstance.save flush: true
        respond workOrderInstance, [status: CREATED]
    }

    @Transactional
    def update(WorkOrder workOrderInstance) {
        if (workOrderInstance == null) {
            render status: NOT_FOUND
            return
        }

        workOrderInstance.validate()
        if (workOrderInstance.hasErrors()) {
            render status: NOT_ACCEPTABLE
            return
        }

        workOrderInstance.save flush: true
        respond workOrderInstance, [status: OK]
    }

    @Transactional
    def delete(WorkOrder workOrderInstance) {

        if (workOrderInstance == null) {
            render status: NOT_FOUND
            return
        }

        workOrderInstance.delete flush: true
        render status: NO_CONTENT
    }

    def findCustomer(){
        def name = params.name

        def customer = Customer.findByName(name)
        if(!customer){
            render status: NOT_ACCEPTABLE
            return
        }


        respond customer , status:OK
    }

    def getCustomerList(){
        def customers = [:]

        def customerList = Customer.getAll()


        render customerList as JSON
    }
    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def booking = WorkOrder.get(id)
                if(booking){
                    booking.delete()
                    deletedContact++
                }

            }catch(Exception ex){
                log.error "WorkOrder Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedWorkOrder",deletedContact)
        responseJson.put("notDeleteWorkOrder",notDeletedContact)
        respond responseJson
    }

    def show(WorkOrder workOrderInstance){
        if (workOrderInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond workOrderInstance
    }
}
