package com.app.front

import grails.converters.JSON

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AssetAppController {

    static responseFormats = ['json', 'xml']
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
def assetAppControllerService
    def index(Integer max) {
       render assetAppControllerService.getList(params) as JSON
    }
    def nameExist(){
        String name = params.name
        if(name){
            def role = AssetApp.findByName(name)
            if(role)
                render status:FOUND
            else
                render status:ACCEPTED
        }
        else
            render status:NOT_ACCEPTABLE

    }
    def show(AssetApp assetAppInstance){
        if (assetAppInstance == null) {
            render status: NOT_FOUND
            return
        }
        respond assetAppInstance
    }
    @Transactional
    def save(AssetApp assetAppInstance) {
        if (assetAppInstance == null) {
            render status: NOT_FOUND
            return
        }

        assetAppInstance.validate()
        if (assetAppInstance.hasErrors()) {
            respond assetAppInstance, status: NOT_ACCEPTABLE
            return
        }

        assetAppInstance.save flush: true
        respond assetAppInstance, [status: CREATED]
    }

    @Transactional
    def update(AssetApp assetAppInstance) {
        if (assetAppInstance == null) {
            render status: NOT_FOUND
            return
        }
         assetAppInstance.validate()
        if (assetAppInstance.hasErrors()) {
            respond assetAppInstance,status: NOT_ACCEPTABLE
            return
        }

        assetAppInstance.save flush: true
        respond assetAppInstance, [status: OK]
    }

    @Transactional
    def delete(AssetApp assetAppInstance) {

        if (assetAppInstance == null) {
            render status: NOT_FOUND
            return
        }

        assetAppInstance.delete flush: true
        render status: NO_CONTENT
    }

    @Transactional
    def deleteSelected(){
        def deletedContact = 0
        def notDeletedContact = 0
        def responseJson = [:]
        def deletedIds = params.getList("ids")
        for (id in deletedIds){
            try{
                def assetApp = AssetApp.get(id)
                if(assetApp){
                    assetApp.delete()
                    deletedContact++
                }

            }catch(Exception ex){
                log.error "Investor Not Deleted : " + ex
                notDeletedContact++
            }


        }
        responseJson.put("deletedAssetApp",deletedContact)
        responseJson.put("notDeleteAssetApp",notDeletedContact)
        respond responseJson
    }
}
