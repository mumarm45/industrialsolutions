package com.app.front

import com.app.security.User
import grails.converters.JSON
import groovy.json.JsonSlurper

/**
 * InvestorControllerService
 * A service class encapsulates the core business logic of a Grails application
 */
class InvestorControllerService {

    static transactional = true

    def getList(params){
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def investorCount = 0
        def query = "From Investor u  "
        if(jsonFiltering.name)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.name LIKE '%${jsonFiltering.name}%'"
            else
                query += " WHERE u.name LIKE '%${jsonFiltering.name}%'"

        }
        if(jsonFiltering.mobileNumber)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.mobileNumber LIKE '%${jsonFiltering.mobileNumber}%'"
            else
                query += " WHERE u.mobileNumber LIKE '%${jsonFiltering.mobileNumber}%'"

        }
        if(jsonFiltering.sirName)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.sirName LIKE '%${jsonFiltering.sirName}%'"
            else
                query += " WHERE u.sirName LIKE '%${jsonFiltering.sirName}%'"

        }
        if(jsonFiltering.email)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.email LIKE '%${jsonFiltering.email}%'"
            else
                query += " WHERE u.email LIKE '%${jsonFiltering.email}%'"

        }
        if(jsonFiltering.status)  {
            def status = true
            if(jsonFiltering.status.toLowerCase().equals("yes"))
                status = true
            else
                status = false

            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.status = '${status}'"
            else
                query += " WHERE u.status = '${status}'"

        }
        investorCount =  Investor.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.name)
            query += " ORDER BY u.name " + jsonSorting.name
        else if(jsonSorting.sirName)
            query += " ORDER BY u.name " + jsonSorting.sirName
        else if(jsonSorting.mobileNumber)
            query += " ORDER BY u.mobileNumber " + jsonSorting.mobileNumber
        else if(jsonSorting.email)
            query += " ORDER BY u.email " + jsonSorting.email
        else if(jsonSorting.status)
            query += " ORDER BY u.status " + jsonSorting.status


        def listFiltered =  Investor.findAll(query,[ max:params.count as int ,
                                                     offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("investorsList", listFiltered)
        def nameFiltered = false

        resultList.put("investorsCount",investorCount)

       return resultList
    }
}
