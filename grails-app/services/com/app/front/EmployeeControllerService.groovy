package com.app.front

import groovy.json.JsonSlurper

/**
 * EmployeeControllerService
 * A service class encapsulates the core business logic of a Grails application
 */
class EmployeeControllerService {

    static transactional = true

    def getList(params){
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def employeeCount = 0
        def query = "From Employee u  "
        if(jsonFiltering.name)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.name LIKE '%${jsonFiltering.name}%'"
            else
                query += " WHERE u.name LIKE '%${jsonFiltering.name}%'"

        }
        if(jsonFiltering.mobileNumberPrimary)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.mobileNumberPrimary LIKE '%${jsonFiltering.mobileNumberPrimary}%'"
            else
                query += " WHERE u.mobileNumberPrimary LIKE '%${jsonFiltering.mobileNumberPrimary}%'"

        }
        if(jsonFiltering.sirName)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.sirName LIKE '%${jsonFiltering.sirName}%'"
            else
                query += " WHERE u.sirName LIKE '%${jsonFiltering.sirName}%'"

        }
        if(jsonFiltering.designation)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.designation LIKE '%${jsonFiltering.designation}%'"
            else
                query += " WHERE u.designation LIKE '%${jsonFiltering.designation}%'"

        }
        if(jsonFiltering.status)  {
            def status = true
            if(jsonFiltering.status.toLowerCase().equals("yes"))
                status = true
            else
                status = false

            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.status = '${status}'"
            else
                query += " WHERE u.status = '${status}'"

        }
        employeeCount =  Employee.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.name)
            query += " ORDER BY u.name " + jsonSorting.name
        else if(jsonSorting.sirName)
            query += " ORDER BY u.sirName " + jsonSorting.sirName
        else if(jsonSorting.mobileNumberPrimary)
            query += " ORDER BY u.mobileNumberPrimary " + jsonSorting.mobileNumberPrimary
        else if(jsonSorting.customerType)
            query += " ORDER BY u.customerType " + jsonSorting.customerType
        else if(jsonSorting.status)
            query += " ORDER BY u.status " + jsonSorting.status


        def listFiltered =  Employee.findAll(query,[ max:params.count as int ,
                                                     offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("employeesList", listFiltered)
        def nameFiltered = false

        resultList.put("employeesCount",employeeCount)

        return resultList
    }
}
