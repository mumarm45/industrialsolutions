package com.app.front

import groovy.json.JsonSlurper

/**
 * CustomerControllerService
 * A service class encapsulates the core business logic of a Grails application
 */
class CustomerControllerService {

    static transactional = true

    def getList(params){
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def customerCount = 0
        def query = "From Customer u  "
        if(jsonFiltering.name)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.name LIKE '%${jsonFiltering.name}%'"
            else
                query += " WHERE u.name LIKE '%${jsonFiltering.name}%'"

        }
        if(jsonFiltering.mobileNumberPrimary)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.mobileNumberPrimary LIKE '%${jsonFiltering.mobileNumberPrimary}%'"
            else
                query += " WHERE u.mobileNumberPrimary LIKE '%${jsonFiltering.mobileNumberPrimary}%'"

        }
        if(jsonFiltering.position)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.position LIKE '%${jsonFiltering.position}%'"
            else
                query += " WHERE u.position LIKE '%${jsonFiltering.position}%'"

        }
        if(jsonFiltering.customerType)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.customerType LIKE '%${jsonFiltering.customerType}%'"
            else
                query += " WHERE u.customerType LIKE '%${jsonFiltering.customerType}%'"

        }
        if(jsonFiltering.status)  {
            def status = true
            if(jsonFiltering.status.toLowerCase().equals("yes"))
                status = true
            else
                status = false

            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.status = '${status}'"
            else
                query += " WHERE u.status = '${status}'"

        }
        customerCount =  Customer.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.name)
            query += " ORDER BY u.name " + jsonSorting.name
        else if(jsonSorting.position)
            query += " ORDER BY u.position " + jsonSorting.position
        else if(jsonSorting.mobileNumberPrimary)
            query += " ORDER BY u.mobileNumberPrimary " + jsonSorting.mobileNumberPrimary
        else if(jsonSorting.customerType)
            query += " ORDER BY u.customerType " + jsonSorting.customerType
        else if(jsonSorting.status)
            query += " ORDER BY u.status " + jsonSorting.status


        def listFiltered =  Customer.findAll(query,[ max:params.count as int ,
                                                     offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("customersList", listFiltered)
        def nameFiltered = false

        resultList.put("customersCount",customerCount)

        return resultList
    }
}
