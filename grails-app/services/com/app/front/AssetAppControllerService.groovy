package com.app.front

import groovy.json.JsonSlurper

/**
 * AssetAppControllerService
 * A service class encapsulates the core business logic of a Grails application
 */
class AssetAppControllerService {

    static transactional = true

    def getList(params){
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def assetAppCount = 0
        def query = "From AssetApp u  "
        if(jsonFiltering.name)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.name LIKE '%${jsonFiltering.name}%'"
            else
                query += " WHERE u.name LIKE '%${jsonFiltering.name}%'"

        }
        if(jsonFiltering.make)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.make LIKE '%${jsonFiltering.make}%'"
            else
                query += " WHERE u.make LIKE '%${jsonFiltering.make}%'"

        }
        if(jsonFiltering.expiryDate)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.expiryDate LIKE '%${jsonFiltering.expiryDate}%'"
            else
                query += " WHERE u.expiryDate LIKE '%${jsonFiltering.expiryDate}%'"

        }

        if(jsonFiltering.warranty)  {
            def warranty = true
            if(jsonFiltering.warranty.toLowerCase().equals("yes"))
                warranty = true
            else
                warranty = false

            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.warranty = '${warranty}'"
            else
                query += " WHERE u.warranty = '${warranty}'"

        }
        assetAppCount =  AssetApp.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.name)
            query += " ORDER BY u.name " + jsonSorting.name
        else if(jsonSorting.make)
            query += " ORDER BY u.make " + jsonSorting.make
        else if(jsonSorting.expiryDate)
            query += " ORDER BY u.expiryDate " + jsonSorting.expiryDate
        else if(jsonSorting.warranty)
            query += " ORDER BY u.warranty " + jsonSorting.warranty


        def listFiltered =  AssetApp.findAll(query,[ max:params.count as int ,
                                                     offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("assetAppsList", listFiltered)
        def nameFiltered = false

        resultList.put("assetAppsCount",assetAppCount)

        return resultList
    }
}
