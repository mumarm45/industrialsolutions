package com.app.front

import groovy.json.JsonSlurper

/**
 * WorkOrderService
 * A service class encapsulates the core business logic of a Grails application
 */
class WorkOrderService {

    static transactional = true

    def getList(params){
        def jsonSorting = new JsonSlurper().parseText(params.sorting)
        def jsonFiltering = new JsonSlurper().parseText(params.filter)
        def resultList = [:]
        def workOrderCount = 0
        def query = "From WorkOrder u  "
        if(jsonFiltering.customerName)  {
            if(query.matches("(.*)WHERE(.*)"))
                query += " AND u.customerName LIKE '%${jsonFiltering.customerName}%'"
            else
                query += " WHERE u.customerName LIKE '%${jsonFiltering.customerName}%'"

        }

        workOrderCount =  WorkOrder.executeQuery("SELECT COUNT(*) "+query)[0]
        if(jsonSorting.customerName)
            query += " ORDER BY u.customerName " + jsonSorting.customerName


        def listFiltered =  WorkOrder.findAll(query,[ max:params.count as int ,
                                                    offset:( ( params.page as int  ) - 1 ) * (params.count as int ) ])
        resultList.put("workOrdersList", listFiltered)
        def nameFiltered = false

        resultList.put("workOrdersCount",workOrderCount)

        return resultList
    }
}
