/**
 * Created by mum on 10/28/2014.
 */

 angular.module('indapp')
  .controller('addNewCustomerController',function($scope,$rootScope,$location,$http,crudOperationCustomer,toaster,$filter){

    $scope.addNew = function(){
        $location.path('/customer/create');
    };

         $scope.customer.customerType = 0;
         $scope.customerTypeList = [
             {value:0,displayName:'Other'},
             {value:1,displayName:'END_USER'},
             {value:2,displayName:'DIE_MAKER'},
             {value:3,displayName:'MIDDLE_MAN'}
         ];

    $scope.viewCustomerList = function(){
        $location.path('/customer/index');
    };
    $scope.alerts = [];

    $scope.saveCustomer = function(invalid){

        if(invalid)
            return;
         if($scope.customer.email=="valid")
            $scope.customer.email = undefined;
        $scope.customerObject = {
            //id:customer.id,
            name:$scope.customer.name,
            position:$scope.customer.position,
            companyName:$scope.customer.companyName,
            customerType:$scope.customer.customerType,
            billingAddress:$scope.customer.billingAddress,
            reference:$scope.customer.reference,
            mobileNumberPrimary:$scope.customer.mobileNumberPrimary,
            mobileNumberSecondary:$scope.customer.mobileNumberSecondary,
            email: $scope.customer.email,
            landLine:$scope.customer.landLine,
            web:$scope.customer.web,
            remark:$scope.customer.remark,
            deActivateReason:$scope.customer.deActivateReason,
            status:$scope.customer.status,
            'updatedBy':$rootScope._user.id,
            'createdBy':$rootScope._user.id
        };
        var params = $scope.customerObject;
        crudOperationCustomer.get_api.save(params,function(response){
          //  $scope.$parent.alerts.push({ type: 'success', msg: 'customer successfully created',timeout:5000});
            toaster.pop('info', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.createSuccessfully"));
            $location.path('/customer/list');

        },
            function(error){
            var errors = [];
            errors = error.data.errors;
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            else{
                for(err in errors){
                    if(errors[err]["field"] == "name")
                    toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")
                    ("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    else if(errors[err]["field"] == "email")
                        toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                   else {
                        toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")
                        ("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    }
                }

            }

        });
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }
  }) ;


