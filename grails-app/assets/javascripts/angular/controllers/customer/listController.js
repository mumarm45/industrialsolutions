/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listCustomerController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationCustomer,$q,$translate) {
        $scope.customers =[];
        $scope.hideShow = false;
        $scope.alerts = [];
        $scope.customerTypeList = [
            {value:0,displayName:'Other'},
            {value:1,displayName:'END_USER'},
            {value:2,displayName:'DIE_MAKER'},
            {value:3,displayName:'MIDDLE_MAN'}
        ];


        var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            }
        $scope.isActiveStatus = function(column) {
            var blockLabel="Block";
            var activeLabel="Active";
            $translate("com.app.ind.user.label.block").then(function(value){
                blockLabel = value ;
            });
            $translate("com.app.ind.user.label.active").then(function(value){
                activeLabel = value ;
            });
            var def = $q.defer();
            var isActiveStatusArray = [];
            isActiveStatusArray.push({'id':'Yes',title:activeLabel});
            isActiveStatusArray.push({'id':'No',title:blockLabel});

            def.resolve(isActiveStatusArray);
            return def;
        };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationCustomer.getList(params.$params).then( function(response){
                            $scope.customers = response.customersList;
                            params.total(response.customersCount);





                            $defer.resolve(response.customersList);

}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedcustomers =function(){
                        return $filter('filter')($scope.customers,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedcustomers().length == $scope.customers.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.customers,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedcustomers(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                    crudOperationcustomer.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.customer.noDeleted",{number:response.notDeletedcustomer}));
                            toaster.pop('success', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.customer.deleted",{number:response.deletedcustomer}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), error.message);

                        })

                });

            } ;


        } ;
        load();
        $scope.alerts = null;
        $scope.editCustomer =function(id){
            $location.path('customer/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('customer/detail/'+id);
        } ;
        $scope.editEntityCustomer = function(data,customer,prop){
            customer[prop]=data;
            var d= $q.defer();
            $scope.customerObject = {
                id:customer.id,
                name:customer.name,
                position:customer.position,
                companyName:customer.companyName,
                customerType:customer.customerType,
                billingAddress:customer.billingAddress,
                reference:customer.reference,
                mobileNumberPrimary:customer.mobileNumberPrimary,
                mobileNumberSecondary:customer.mobileNumberSecondary,
                email: customer.email,
                landline:customer.landline,
                web:customer.web,
                remark:customer.remark,
                deActivateReason:customer.deActivateReason,
                status:customer.status,
                'updatedBy':$rootScope._user.id
            };
            crudOperationCustomer.update($scope.customerObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.customer"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockCustomer = function(data,customer,prop){
            customer[prop]=data;
            var d= $q.defer();

            crudOperationCustomer.activeAndBlockCustomer({id:customer.id,checked:customer.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.customer"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.deleteCustomer = function(index){
            var toDel = $scope.customers[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationCustomer.get_api.delete(toDel,function(response){
                    $scope.customers.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.customer"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.customer"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        }
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        }
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationCustomer.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.customer.noDeleted",{number:response.notDeletedCustomer}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.customer.deleted",{number:response.deletedCustomer}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailCustomer = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/customer/detail.html',
                controller: 'detailcustomerController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
