/**
 * Created by Umar on 11/10/14.
 */

angular.module('indapp')
    .controller('detailCustomerController',function($rootScope,$scope,$http,crudOperationCustomer,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.customerId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};
        $scope.customerTypeList = [
            {value:0,displayName:'Other'},
            {value:1,displayName:'END_USER'},
            {value:2,displayName:'DIE_MAKER'},
            {value:3,displayName:'MIDDLE_MAN'}
        ];
        crudOperationCustomer.show({id: $scope.customerId}).then(function (response) {

            $scope.customer = response;
            console.log($scope.customer.createdBy.id);
            $scope.showUser($scope.customer.createdBy.id,'createdBy');
            $scope.showUser($scope.customer.updatedBy.id,'updatedBy');

        }, function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });

        $scope.editEntityCustomer = function(data,customer,prop){
            customer[prop]=data;
            var d= $q.defer();
            $scope.customerObject = {
                id:customer.id,
                name:customer.name,
                position:customer.position,
                companyName:customer.companyName,
                customerType:customer.customerType,
                billingAddress:customer.billingAddress,
                reference:customer.reference,
                mobileNumberPrimary:customer.mobileNumberPrimary,
                mobileNumberSecondary:customer.mobileNumberSecondary,
                email: customer.email,
                landline:customer.landline,
                web:customer.web,
                remark:customer.remark,
                deActivateReason:customer.deActivateReason,
                status:customer.status,
                'updatedBy':$rootScope._user.id
            };
            crudOperationCustomer.update($scope.customerObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.customer"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.customer"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
             crudOperationUser.show({ id: userId }).then(function (response) {
                 if(val == 'createdBy')
                 $scope.createdByUser = response;
                 else
                 $scope.updatedByUser = response;
                 d.resolve(response);

            }, function (error) {
                 d.reject(error);
            });
            return d.promise;
        };
    });
