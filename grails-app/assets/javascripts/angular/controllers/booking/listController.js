/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listBookingController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationBooking,$q,$translate) {
        $scope.bookings =[];
        $scope.hideShow = false;
        $scope.alerts = [];

        $scope.getCustomerList =  function(){
            var d= $q.defer();
            crudOperationBooking.getCustomerList().then(

                function(respose){
                    $scope.customerList = respose;

                    d.resolve();

                },
                function(error){
                    d.reject();
                    toaster.pop('error', "Error while getting customer list");
                });

            return d.promise;
        };

        $scope.getCustomerList();

         var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationBooking.getList(params.$params).then( function(response){
                            $scope.bookings = response.bookingsList;
                            params.total(response.bookingsCount);


                            $scope.convertStringToDate();


                            $defer.resolve(response.bookingsList);

}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedbookings =function(){
                        return $filter('filter')($scope.bookings,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedbookings().length == $scope.bookings.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.bookings,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedbookings(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                    crudOperationBooking.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.booking.noDeleted",{number:response.notDeletedbooking}));
                            toaster.pop('success', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.booking.deleted",{number:response.deletedbooking}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.app.ind.label.booking"), error.message);

                        })

                });

            } ;


        } ;
        load();
        $scope.alerts = null;
        $scope.editBooking =function(id){
            $location.path('booking/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('booking/detail/'+id);
        } ;
        $scope.editEntityBooking = function(data,booking,prop){
            booking[prop]=data;
            var d= $q.defer();
            $scope.bookingObject = {
                id:booking.id,
                bookingOrder:booking.bookingOrder,
                customerName:booking.customerName,
                customer:booking.customer,
                estimatedTime:booking.estimatedTime,
                description:booking.description,
                committedDate:booking.committedDate,
                'updatedBy':$rootScope._user.id
            };
            crudOperationBooking.update($scope.bookingObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.booking"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.convertStringToDate = function(){
            angular.forEach($scope.bookings,function(val,key){
                //console.log(val);
                if(val['bookingDate']){
                    $scope.bookings[key]['bookingDate']  = new Date(val['bookingDate']);
                }
                if(val['committedDate']){
                    $scope.bookings[key]['committedDate']  = new Date(val['committedDate']);
                }
                if(val['estimatedTime']){
                    var dateTime =  new Date(val['estimatedTime']);
                    $scope.bookings[key]['estimatedTime']  =dateTime.getUTCHours()+":" +dateTime.getUTCMinutes();
                }
            })
        };

        $scope.deleteBooking = function(index){
            var toDel = $scope.bookings[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationBooking.get_api.delete(toDel,function(response){
                    $scope.bookings.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.booking"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.booking"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        };
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        };
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationBooking.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.booking.noDeleted",{number:response.notDeletedBooking}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.booking.deleted",{number:response.deletedBooking}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailBooking = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/booking/detail.html',
                controller: 'detailbookingController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
