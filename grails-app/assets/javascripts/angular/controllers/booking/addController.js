/**
 * Created by mum on 10/28/2014.
 */

 angular.module('indapp')
  .controller('addNewBookingController',function($q,$scope,$rootScope,$location,$http,crudOperationBooking,toaster,$filter){

    $scope.addNew = function(){
        $location.path('/booking/create');
    };

         $scope.booking.bookingType = 0;


         $scope.getCustomerList =  function(){
             var d= $q.defer();
             crudOperationBooking.getCustomerList().then(

                 function(respose){
                     $scope.customerList = respose;

                     d.resolve();

                 },
                 function(error){
                     d.reject();
                     toaster.pop('error', "Error while getting customer list");
                 });

             return d.promise;
         };

         $scope.getCustomerList();
    $scope.viewBookingList = function(){
        $location.path('/booking/index');
    };
    $scope.alerts = [];
         $scope.customerFound = '';
         $scope.findCustomer = function(customerName){
             if(customerName){
                 crudOperationBooking.findCustomer({name:customerName}).then(
                     function(respose){
                         $scope.customerFound = respose.name;
                         $scope.customer = respose.id;

                     },
                     function(error){

                     });
                 // return d.promise;
             }

         };

    $scope.saveBooking = function(invalid){

        if(invalid)
            return;

        $scope.bookingObject = {
            //id:booking.id,
            bookingDate:$scope.booking.bookingDate,
            customerName:$scope.booking.customer_id,
            estimatedTime:$scope.booking.estimatedTime,
            customer:$scope.booking.customer_id,
            description:$scope.booking.description,
            committedDate:$scope.booking.committedDate,
            'updatedBy':$rootScope._user.id,
            'createdBy':$rootScope._user.id
        };
        var params = $scope.bookingObject;


        crudOperationBooking.get_api.save(params,function(response){
          //  $scope.$parent.alerts.push({ type: 'success', msg: 'booking successfully created',timeout:5000});
            toaster.pop('info', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.createSuccessfully"));
            $location.path('/booking/list');

        },
            function(error){
            var errors = [];
            errors = error.data.errors;
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            else{
                for(err in errors){
                    if(errors[err]["field"] == "name")
                    toaster.pop('error', $filter("translate")("com.app.ind.label.booking"), $filter("translate")
                    ("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    else if(errors[err]["field"] == "email")
                        toaster.pop('error', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                   else {
                        toaster.pop('error', 'Filed' + errors[err]["field"] + '\n message'+errors[err]["message"]);
                    }
                }

            }

        });
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }
  }) ;


