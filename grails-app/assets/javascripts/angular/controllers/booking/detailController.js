/**
 * Created by Umar on 11/10/14.
 */

angular.module('indapp')
    .controller('detailBookingController',function($rootScope,$scope,$http,crudOperationBooking,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.bookingId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};
        $scope.getCustomerList =  function(){
            var d= $q.defer();
            crudOperationBooking.getCustomerList().then(

                function(respose){
                    $scope.customerList = respose;

                    d.resolve();

                },
                function(error){
                    d.reject();
                    toaster.pop('error', "Error while getting customer list");
                });

            return d.promise;
        };

        $scope.getCustomerList();
        crudOperationBooking.show({id: $scope.bookingId}).then(function (response) {

            $scope.booking = response;
            $scope.convertStringToDate();
            $scope.showUser($scope.booking.createdBy.id,'createdBy');
            $scope.showUser($scope.booking.updatedBy.id,'updatedBy');

        }, function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });
        $scope.convertStringToDate = function() {
            if($scope.booking.bookingDate){
                $scope.booking.bookingDate = new Date($scope.booking.bookingDate);
            }
            if($scope.booking.committedDate){
                $scope.booking.committedDate  = new Date($scope.booking.committedDate);
            }
            if($scope.booking.estimatedTime){
                var dateTime =  new Date($scope.booking.estimatedTime);
                $scope.booking.estimatedTime  =dateTime.getUTCHours()+":" +dateTime.getUTCMinutes();
            }
        };
        $scope.editEntityBooking = function(data,booking,prop){
            booking[prop]=data;
            var d= $q.defer();
            $scope.bookingObject = {
 id:booking.id,
                bookingOrder:booking.bookingOrder,
                customerName:booking.customerName,
                customer:booking.customer,
                estimatedTime:booking.estimatedTime,
                description:booking.description,
                committedDate:booking.committedDate,
                'updatedBy':$rootScope._user.id
            };
            crudOperationBooking.update($scope.bookingObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.booking"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.booking"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
             crudOperationUser.show({ id: userId }).then(function (response) {
                 if(val == 'createdBy')
                 $scope.createdByUser = response;
                 else
                 $scope.updatedByUser = response;
                 d.resolve(response);

            }, function (error) {
                 d.reject(error);
            });
            return d.promise;
        };
    });
