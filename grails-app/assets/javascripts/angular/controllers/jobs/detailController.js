angular.module('indapp')
    .controller('detailJobsController',function($rootScope,$scope,$http,crudOperationJobs,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.jobsId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};
        $scope.getEmployeeList =  function(){
            var d= $q.defer();
            crudOperationJobs.getEmployeeList().then(

                function(respose){
                    $scope.employeeList = respose;

                    d.resolve();

                },
                function(error){
                    d.reject();
                    toaster.pop('error', "Error while getting customer list");
                });

            return d.promise;
        };

        $scope.getEmployeeList();
        crudOperationJobs.show({id: $scope.jobsId}).then(function (response) {
            $scope.jobs = response;
            $scope.convertStringToDate();
            $scope.showUser($scope.jobs.createdBy.id,'createdBy');
            $scope.showUser($scope.jobs.updatedBy.id,'updatedBy');

        },
            function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });
        $scope.convertStringToDate = function() {
            if($scope.jobs.jobsDate){
                $scope.jobs.jobsDate = new Date($scope.jobs.jobsDate);
            }
            if($scope.jobs.committedDate){
                $scope.jobs.committedDate  = new Date($scope.jobs.committedDate);
            }
            if($scope.jobs.estimatedTime){
                var dateTime =  new Date($scope.jobs.estimatedTime);
                $scope.jobs.estimatedTime  =dateTime.getUTCHours()+":" +dateTime.getUTCMinutes();
            }
            if($scope.jobs.endTime){
                var endTime =  new Date($scope.jobs.endTime);
                $scope.jobs.endTime  =endTime.getUTCHours()+":" +endTime.getUTCMinutes();
            }
            if($scope.jobs.startTime){
                var startTime =  new Date($scope.jobs.startTime);
                $scope.jobs.startTime  =startTime.getUTCHours()+":" +startTime.getUTCMinutes();
            }
        };
        $scope.editEntityJobs = function(data,jobs,prop){
            jobs[prop]=data;
            var d= $q.defer();
            jobs.wcTotal = 1;
            if(jobs.wcLength!=undefined)
                jobs.wcTotal = jobs.wcLength ;
            if( jobs.wcHeight !=undefined)
                jobs.wcTotal = jobs.wcTotal*  jobs.wcHeight ;
            if(jobs.wcQuantityHours !=undefined)
                jobs.wcTotal = jobs.wcTotal*  jobs.wcQuantityHours ;
            if(jobs.wcRate !=undefined)
                jobs.wcTotal = jobs.wcTotal*  jobs.wcRate ;
            $scope.jobsObject = {
                id:jobs.id,
                jobNumber:jobs.jobNumber,
                jobTitle:jobs.jobTitle,
                machineFileName:jobs.machineFileName,
                programedBy:jobs.programedBy,
                settingsBy:jobs.settingsBy,
                checkedBy:jobs.checkedBy,
                assignTo:jobs.assignTo,
                committedDate:jobs.committedDate,
                approxTime:jobs.approxTime,
                startTime:jobs.startTime,
                endTime:jobs.endTime,
                markingOnJob:jobs.markingOnJob,
                mirror:jobs.mirror,
                clearance:jobs.clearance,
                offSet:jobs.offSet,
                programSize:jobs.programSize,
                originalSize:jobs.originalSize,
                saleAccountName:jobs.saleAccountName,
                saleAccountNo:jobs.saleAccountNo,
                wcLength:jobs.wcLength,
                wcHeight:jobs.wcHeight,
                wcQuantityHours:jobs.wcQuantityHours,
                wcRate:jobs.wcRate,
                wcTotal:jobs.wcTotal,
                currentRatio:jobs.currentRatio,
                priority:jobs.priority,
                customerComments:jobs.customerComments,
                operatorComments:jobs.operatorComments,

                'updatedBy':$rootScope._user.id
            };
            crudOperationJobs.update($scope.jobsObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.jobs"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
            crudOperationUser.show({ id: userId }).then(function (response) {
                if(val == 'createdBy')
                    $scope.createdByUser = response;
                else
                    $scope.updatedByUser = response;
                d.resolve(response);

            }, function (error) {
                d.reject(error);
            });
            return d.promise;
        };
    });
