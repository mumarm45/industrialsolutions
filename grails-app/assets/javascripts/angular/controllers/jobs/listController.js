/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listJobsController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationJobs,$q,$translate) {
        $scope.jobss =[];
        $scope.hideShow = false;
        $scope.alerts = [];

       /* $scope.getCustomerList =  function(){
            var d= $q.defer();
            crudOperationJobs.getEmployeeList().then(

                function(respose){
                    $scope.emplpyeeList = respose;

                    d.resolve();

                },
                function(error){
                    d.reject();
                    toaster.pop('error', "Error while getting customer list");
                });

            return d.promise;
        };

        $scope.getCustomerList();*/

         var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationJobs.getList(params.$params).then( function(response){
                            $scope.jobss = response.jobssList;
                            params.total(response.jobssCount);


                            $scope.convertStringToDate();


                            $defer.resolve(response.jobssList);

}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedjobss =function(){
                        return $filter('filter')($scope.jobss,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedjobss().length == $scope.jobss.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.jobss,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedjobss(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                    crudOperationJobs.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.jobs.noDeleted",{number:response.notDeletedjobs}));
                            toaster.pop('success', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.jobs.deleted",{number:response.deletedjobs}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.app.ind.label.jobs"), error.message);

                        })

                });

            } ;


        } ;
        load();
        $scope.alerts = null;
        $scope.editJobs =function(id){
            $location.path('jobs/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('jobs/detail/'+id);
        } ;
        $scope.editEntityJobs = function(data,jobs,prop){
            jobs[prop]=data;
            var d= $q.defer();
            $scope.jobsObject = {
                id:jobs.id,
                jobsOrder:jobs.jobsOrder,
                customerName:jobs.customerName,
                customer:jobs.customer,
                estimatedTime:jobs.estimatedTime,
                description:jobs.description,
                committedDate:jobs.committedDate,
                'updatedBy':$rootScope._user.id
            };
            crudOperationJobs.update($scope.jobsObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.jobs"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.convertStringToDate = function(){
            angular.forEach($scope.jobss,function(val,key){
                //console.log(val);
                if(val['jobsDate']){
                    $scope.jobss[key]['jobsDate']  = new Date(val['jobsDate']);
                }
                if(val['committedDate']){
                    $scope.jobss[key]['committedDate']  = new Date(val['committedDate']);
                }
                if(val['estimatedTime']){
                    var dateTime =  new Date(val['estimatedTime']);
                    $scope.jobss[key]['estimatedTime']  =dateTime.getUTCHours()+":" +dateTime.getUTCMinutes();
                }
            })
        };

        $scope.deleteJobs = function(index){
            var toDel = $scope.jobss[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationJobs.get_api.delete(toDel,function(response){
                    $scope.jobss.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.jobs"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.jobs"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        };
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        };
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationJobs.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.jobs.noDeleted",{number:response.notDeletedJobs}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.jobs"), $filter("translate")("com.app.ind.label.jobs.deleted",{number:response.deletedJobs}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailJobs = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/jobs/detail.html',
                controller: 'detailjobsContro ller',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
