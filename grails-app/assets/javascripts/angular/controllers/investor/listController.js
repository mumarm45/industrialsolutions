/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listInvestorController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationInvestor,$q,$translate) {
        $scope.investors =[];
        $scope.hideShow = false;
        $scope.alerts = [];
        var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            }
        $scope.isActiveStatus = function(column) {
            var blockLabel="Block";
            var activeLabel="Active";
            $translate("com.app.ind.user.label.block").then(function(value){
                blockLabel = value ;
            });
            $translate("com.app.ind.user.label.active").then(function(value){
                activeLabel = value ;
            });
            var def = $q.defer();
            var isActiveStatusArray = [];
            isActiveStatusArray.push({'id':'Yes',title:activeLabel});
            isActiveStatusArray.push({'id':'No',title:blockLabel});

            def.resolve(isActiveStatusArray);
            return def;
        };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationInvestor.getList(params.$params).then( function(response){
                            $scope.investors = response.investorsList;
                            params.total(response.investorsCount);
                            $defer.resolve(response.investorsList);
}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedinvestors =function(){
                        return $filter('filter')($scope.investors,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedinvestors().length == $scope.investors.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.investors,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedinvestors(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                    crudOperationinvestor.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.investor.noDeleted",{number:response.notDeletedinvestor}));
                            toaster.pop('success', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.investor.deleted",{number:response.deletedinvestor}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), error.message);

                        })

                });

            } ;


        } ;
        load();
        $scope.alerts = null;
        $scope.editInvestor =function(id){
            $location.path('investor/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('investor/detail/'+id);
        } ;
        $scope.editEntityInvestor = function(data,investor,prop){
            investor[prop]=data;
            var d= $q.defer();
            $scope.investorObject = {
                id:investor.id,
                name:investor.name,
                sirName:investor.sirName,
                address:investor.address,
                reference:investor.reference,
                mobileNumber:investor.mobileNumber,
                email: investor.email,
                bankName:investor.bankName,
                branchName:investor.branchName,
                branchCode:investor.branchCode,
                bankAccountType:investor.bankAccountType,
                bankAccount:investor.bankAccount,
                deActivateReason:investor.deActivateReason,
                investedAmount:investor.investedAmount,
                amountInPercentage:investor.amountInPercentage,
                status:investor.status,
                'updatedBy':$rootScope._user.id
            };
            crudOperationInvestor.update($scope.investorObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.investor"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockInvestor = function(data,investor,prop){
            investor[prop]=data;
            var d= $q.defer();

            crudOperationInvestor.activeAndBlockInvestor({id:investor.id,checked:investor.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.investor"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.deleteInvestor = function(index){
            var toDel = $scope.investors[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationInvestor.get_api.delete(toDel,function(response){
                    $scope.investors.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.investor"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.investor"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        }
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        }
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationInvestor.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.investor.noDeleted",{number:response.notDeletedInvestor}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.investor.deleted",{number:response.deletedInvestor}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailInvestor = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/investor/detail.html',
                controller: 'detailinvestorController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
