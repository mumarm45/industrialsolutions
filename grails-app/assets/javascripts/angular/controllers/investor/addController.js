/**
 * Created by mum on 10/28/2014.
 */

 angular.module('indapp')
  .controller('addNewInvestorController',function($scope,$rootScope,$location,$http,crudOperationInvestor,toaster,$filter){

    $scope.addNew = function(){
        $location.path('/investor/create');
    };

    $scope.viewInvestorList = function(){
        $location.path('/investor/index');
    };
    $scope.alerts = [];
    $scope.saveInvestor = function(invalid){

        if(invalid)
            return;
        console.log($scope.investor);
        if($scope.investor.email=="valid")
            $scope.investor.email = undefined;
        $scope.investorObject = {
            name:$scope.investor.name,
            sirName:$scope.investor.sirName,
            address:$scope.investor.address,
            reference:$scope.investor.reference,
            mobileNumber:$scope.investor.mobileNumber,
            email: $scope.investor.email,
            bankName: $scope.investor.bankName,
            branchName:$scope.investor.branchName,
            branchCode:$scope.investor.branchCode,
            bankAccountType:$scope.investor.bankAccountType,
            bankAccount:$scope.investor.bankAccount,
            deActivateReason:$scope.investor.deActivateReason,
            investedAmount:$scope.investor.investedAmount,
            amountInPercentage:$scope.investor.amountInPercentage,
            status:$scope.investor.status,
            'createdBy':$rootScope._user.id,
            'updatedBy':$rootScope._user.id
        };
        var params = $scope.investorObject;
        crudOperationInvestor.get_api.save(params,function(response){
          //  $scope.$parent.alerts.push({ type: 'success', msg: 'investor successfully created',timeout:5000});
            toaster.pop('info', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.createSuccessfully"));
            $location.path('/investor/list');

        },
            function(error){
            var errors = [];
            errors = error.data.errors;
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            else{
                for(err in errors){
                    if(errors[err]["field"] == "investorname")
                    toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    if(errors[err]["field"] == "email")
                        toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                }

            }

        });
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }
  }) ;


