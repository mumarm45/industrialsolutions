/**
 * Created by Umar on 11/10/14.
 */

angular.module('indapp')
    .controller('detailInvestorController',function($rootScope,$scope,$http,crudOperationInvestor,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.investorId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};

        crudOperationInvestor.show({id: $scope.investorId}).then(function (response) {

            $scope.investor = response;
            $scope.showUser($scope.investor.createdBy.id,'createdBy');
            $scope.showUser($scope.investor.updatedBy.id,'updatedBy');

        }, function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });

        $scope.editEntityInvestor = function(data,investor,prop){
            investor[prop]=data;
            var d= $q.defer();
            $scope.investorObject = {
                id:investor.id,
                name:investor.name,
                sirName:investor.sirName,
                address:investor.address,
                reference:investor.reference,
                mobileNumber:investor.mobileNumber,
                email: investor.email,
                bankName:investor.bankName,
                branchName:investor.branchName,
                branchCode:investor.branchCode,
                bankAccountType:investor.bankAccountType,
                bankAccount:investor.bankAccount,
                deActivateReason:investor.deActivateReason,
                investedAmount:investor.investedAmount,
                amountInPercentage:investor.amountInPercentage,
                status:investor.status,
                'updatedBy':$rootScope._user.id
            };
            crudOperationInvestor.update($scope.investorObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.investor"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.investor"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
             crudOperationUser.show({ id: userId }).then(function (response) {
                 if(val == 'createdBy')
                 $scope.createdByUser = response;
                 else
                 $scope.updatedByUser = response;
                 d.resolve(response);

            }, function (error) {
                 d.reject(error);
            });
            return d.promise;
        };
    });
