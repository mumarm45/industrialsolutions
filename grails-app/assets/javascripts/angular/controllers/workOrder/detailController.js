/**
 * Created by Umar on 11/10/14.
 */

angular.module('indapp')
    .controller('detailWorkOrderController',function($rootScope,$scope,$http,crudOperationWorkOrder,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.workOrderId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};
        $scope.getCustomerList =  function(){
            var d= $q.defer();
            crudOperationWorkOrder.getCustomerList().then(

                function(respose){
                    $scope.customerList = respose;

                    d.resolve();

                },
                function(error){
                    d.reject();
                    toaster.pop('error', "Error while getting customer list");
                });

            return d.promise;
        };

        $scope.getCustomerList();
        crudOperationWorkOrder.show({id: $scope.workOrderId}).then(function (response) {

            $scope.workOrder = response;
            $scope.convertStringToDate();
            $scope.showUser($scope.workOrder.createdBy.id,'createdBy');
            $scope.showUser($scope.workOrder.updatedBy.id,'updatedBy');

        }, function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });
        $scope.convertStringToDate = function() {
            if($scope.workOrder.bookingDate){
                $scope.workOrder.bookingDate = new Date($scope.workOrder.bookingDate);
            }
            if($scope.workOrder.committedDate){
                $scope.workOrder.committedDate  = new Date($scope.workOrder.committedDate);
            }
        };
        $scope.editEntityWorkOrder = function(data,workOrder,prop){
            workOrder[prop]=data;
            var d= $q.defer();
            $scope.workOrderObject = {
                id:workOrder.id,
                bookingDate:workOrder.bookingDate,
                workOrderName:workOrder.workOrderName,
                customer:workOrder.customer_id,
                committedDate:workOrder.committedDate,
                'updatedBy':$rootScope._user.id
            };
            crudOperationWorkOrder.update($scope.workOrderObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.workOrder"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.workOrder"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
            crudOperationUser.show({ id: userId }).then(function (response) {
                if(val == 'createdBy')
                    $scope.createdByUser = response;
                else
                    $scope.updatedByUser = response;
                d.resolve(response);

            }, function (error) {
                d.reject(error);
            });
            return d.promise;
        };
    });
