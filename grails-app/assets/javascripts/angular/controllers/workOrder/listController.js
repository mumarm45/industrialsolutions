/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listWorkOrderController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationWorkOrder,$q,$translate) {
        $scope.workOrders =[];
        $scope.hideShow = false;
        $scope.alerts = [];

        $scope.getCustomerList =  function(){
            var d= $q.defer();
            crudOperationWorkOrder.getCustomerList().then(

                function(respose){
                    $scope.customerList = respose;

                    d.resolve();

                },
                function(error){
                    d.reject();
                    toaster.pop('error', "Error while getting customer list");
                });

            return d.promise;
        };

        $scope.getCustomerList();

         var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationWorkOrder.getList(params.$params).then( function(response){
                            $scope.workOrders = response.workOrdersList;
                            params.total(response.workOrdersCount);

                           $scope.convertStringToDate();



                            $defer.resolve(response.workOrdersList);

}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.workOrder"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedworkOrders =function(){
                        return $filter('filter')($scope.workOrders,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedworkOrders().length == $scope.workOrders.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.workOrders,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedworkOrders(),'id');
                    };

                }
            });



        } ;
        load();
        $scope.convertStringToDate = function(){
          angular.forEach($scope.workOrders,function(val,key){
              //console.log(val);
              if(val['bookingDate']){
                  $scope.workOrders[key]['bookingDate']  = new Date(val['bookingDate']);
              }
              if(val['committedDate']){
                  $scope.workOrders[key]['committedDate']  = new Date(val['committedDate']);
              }
          })
        };
        $scope.alerts = null;
        $scope.editWorkOrder =function(id){
            $location.path('workOrder/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('workOrder/detail/'+id);
        } ;
        $scope.editEntityWorkOrder = function(data,workOrder,prop){
            workOrder[prop]=data;
            var d= $q.defer();
            $scope.workOrderObject = {
                id:workOrder.id,
                bookingDate:workOrder.bookingDate,
                customerName:workOrder.customerName,
                customer:workOrder.customer,
                committedDate:workOrder.committedDate,
                'updatedBy':$rootScope._user.id
            };
            crudOperationWorkOrder.update($scope.workOrderObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.workOrder"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.workOrder"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockWorkOrder = function(data,workOrder,prop){
            workOrder[prop]=data;
            var d= $q.defer();

            crudOperationWorkOrder.activeAndBlockWorkOrder({id:workOrder.id,checked:workOrder.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.workOrder"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.workOrder"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.deleteWorkOrder = function(index){
            var toDel = $scope.workOrders[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationWorkOrder.get_api.delete(toDel,function(response){
                    $scope.workOrders.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.workOrder"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.workOrder"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        };
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        };
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationWorkOrder.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.workOrder"), $filter("translate")("com.app.ind.label.workOrder.noDeleted",{number:response.notDeletedWorkOrder}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.workOrder"), $filter("translate")("com.app.ind.label.workOrder.deleted",{number:response.deletedWorkOrder}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailWorkOrder = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/workOrder/detail.html',
                controller: 'detailworkOrderController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
