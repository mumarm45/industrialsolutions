/**
 * Created by mum on 10/28/2014.
 */

 angular.module('indapp')
  .controller('addNewEmployeeController',function($scope,$rootScope,$location,$http,crudOperationEmployee,toaster,$filter){

    $scope.addNew = function(){
        $location.path('/employee/create');
    };

    $scope.viewEmployeeList = function(){
        $location.path('/employee/index');
    };
    $scope.alerts = [];

    $scope.saveEmployee = function(invalid){

        if(invalid)
            return;
     //   console.log($scope.employee);
        if($scope.employee.email=="valid")
            $scope.employee.email = undefined;
        $scope.employeeObject = {
            name:$scope.employee.name,
            sirName:$scope.employee.sirName,
            fullName:$scope.employee.fullName,
            address:$scope.employee.address,
            reference:$scope.employee.reference,
            mobileNumberPrimary:$scope.employee.mobileNumberPrimary,
            mobileNumberSecondary:$scope.employee.mobileNumberSecondary,
            designation:$scope.employee.designation,
            dateOfBirth:$scope.employee.dateOfBirth,
            salary:$scope.employee.salary,
            idCard:$scope.employee.idCard,
            deActivateReason:$scope.employee.deActivateReason,
            status:$scope.employee.status,
            'createdBy':$rootScope._user.id,
            'updatedBy':$rootScope._user.id
        };
        var params = $scope.employeeObject;
        crudOperationEmployee.get_api.save(params,function(response){
          //  $scope.$parent.alerts.push({ type: 'success', msg: 'employee successfully created',timeout:5000});
            toaster.pop('info', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.createSuccessfully"));
            $location.path('/employee/list');

        },
            function(error){
            var errors = [];
            errors = error.data.errors;
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            else{
                for(err in errors){
                    if(errors[err]["field"] == "employeename")
                    toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    if(errors[err]["field"] == "email")
                        toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                }

            }

        });
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }
  }) ;


