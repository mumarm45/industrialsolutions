/**
 * Created by Umar on 11/10/14.
 */

angular.module('indapp')
    .controller('detailEmployeeController',function($rootScope,$scope,$http,crudOperationEmployee
        ,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.employeeId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};

        crudOperationEmployee.show({id: $scope.employeeId}).then(function (response) {

            $scope.employee = response;
            $scope.showUser($scope.employee.createdBy.id,'createdBy');
            $scope.showUser($scope.employee.updatedBy.id,'updatedBy');

        }, function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });

        $scope.editEntityEmployee = function(data,employee,prop){
            employee[prop]=data;
            var d= $q.defer();
            $scope.employeeObject = {
                id:employee.id,
                name:employee.name,
                sirName:employee.sirName,
                fullName:employee.fullName,
                address:employee.address,
                reference:employee.reference,
                mobileNumberPrimary:employee.mobileNumberPrimary,
                mobileNumberSecondary:employee.mobileNumberSecondary,
                designation:employee.designation,
                dateOfBirth:employee.dateOfBirth,
                salary:employee.salary,
                idCard:employee.idCard,
                deActivateReason:employee.deActivateReason,
                status:employee.status,
                'createdBy':$rootScope._user.id,
                'updatedBy':$rootScope._user.id
            };
            crudOperationEmployee.update($scope.employeeObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.employee"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
             crudOperationUser.show({ id: userId }).then(function (response) {
                 if(val == 'createdBy')
                 $scope.createdByUser = response;
                 else
                 $scope.updatedByUser = response;
                 d.resolve(response);

            }, function (error) {
                 d.reject(error);
            });
            return d.promise;
        };
    });
