/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listEmployeeController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationEmployee,$q,$translate) {
        $scope.employees =[];
        $scope.hideShow = false;
        $scope.alerts = [];
        var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            }
        $scope.isActiveStatus = function(column) {
            var blockLabel="Block";
            var activeLabel="Active";
            $translate("com.app.ind.user.label.block").then(function(value){
                blockLabel = value ;
            });
            $translate("com.app.ind.user.label.active").then(function(value){
                activeLabel = value ;
            });
            var def = $q.defer();
            var isActiveStatusArray = [];
            isActiveStatusArray.push({'id':'Yes',title:activeLabel});
            isActiveStatusArray.push({'id':'No',title:blockLabel});

            def.resolve(isActiveStatusArray);
            return def;
        };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationEmployee.getList(params.$params).then( function(response){
                            $scope.employees = response.employeesList;
                            params.total(response.employeesCount);
                            $defer.resolve(response.employeesList);
}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedemployees =function(){
                        return $filter('filter')($scope.employees,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedemployees().length == $scope.employees.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.employees,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedemployees(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                    crudOperationemployee.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.employee.noDeleted",{number:response.notDeletedemployee}));
                            toaster.pop('success', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.employee.deleted",{number:response.deletedemployee}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), error.message);

                        })

                });

            } ;


        } ;
        load();
        $scope.alerts = null;
        $scope.editEmployee =function(id){
            $location.path('employee/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('employee/detail/'+id);
        } ;
        $scope.editEntityEmployee = function(data,employee,prop){
            employee[prop]=data;
            var d= $q.defer();
            $scope.employeeObject = {
                id:employee.id,
                name:employee.name,
                sirName:employee.sirName,
                fullName:employee.fullName,
                address:employee.address,
                reference:employee.reference,
                mobileNumberPrimary:employee.mobileNumberPrimary,
                mobileNumberSecondary:employee.mobileNumberSecondary,
                designation:employee.designation,
                dateOfBirth:employee.dateOfBirth,
                salary:employee.salary,
                idCard:employee.idCard,
                deActivateReason:employee.deActivateReason,
                status:employee.status,
                'updatedBy':$rootScope._user.id
            };
            crudOperationEmployee.update($scope.employeeObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.employee"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockEmployee = function(data,employee,prop){
            employee[prop]=data;
            var d= $q.defer();

            crudOperationEmployee.activeAndBlockEmployee({id:employee.id,checked:employee.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.employee"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.deleteEmployee = function(index){
            var toDel = $scope.employees[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationEmployee.get_api.delete(toDel,function(response){
                    $scope.employees.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.employee"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.employee"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        }
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        }
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationEmployee.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.employee.noDeleted",{number:response.notDeletedEmployee}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.employee"), $filter("translate")("com.app.ind.label.employee.deleted",{number:response.deletedEmployee}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailEmployee = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/employee/detail.html',
                controller: 'detailemployeeController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
