/**
 * Created by mum on 10/28/2014.
 */

 angular.module('indapp')
  .controller('addNewAssetAppController',function($scope,$rootScope,$location,$http,crudOperationAssetApp,toaster,$filter){

    $scope.addNew = function(){
        $location.path('/assetApp/create');
    };

         $scope.assetApp.assetAppType = 0;


    $scope.viewAssetAppList = function(){
        $location.path('/assetApp/index');
    };
    $scope.alerts = [];

    $scope.saveAssetApp = function(invalid){

        if(invalid)
            return;
         if($scope.assetApp.email=="valid")
            $scope.assetApp.email = undefined;
        $scope.assetAppObject = {
            //id:assetApp.id,
            name:$scope.assetApp.name,
            position:$scope.assetApp.position,
            companyName:$scope.assetApp.companyName,
            assetAppType:$scope.assetApp.assetAppType,
            billingAddress:$scope.assetApp.billingAddress,
            reference:$scope.assetApp.reference,
            mobileNumberPrimary:$scope.assetApp.mobileNumberPrimary,
            mobileNumberSecondary:$scope.assetApp.mobileNumberSecondary,
            email: $scope.assetApp.email,
            landLine:$scope.assetApp.landLine,
            web:$scope.assetApp.web,
            remark:$scope.assetApp.remark,
            deActivateReason:$scope.assetApp.deActivateReason,
            status:$scope.assetApp.status,
            'updatedBy':$rootScope._user.id,
            'createdBy':$rootScope._user.id
        };
        var params = $scope.assetAppObject;
        crudOperationAssetApp.get_api.save(params,function(response){
          //  $scope.$parent.alerts.push({ type: 'success', msg: 'assetApp successfully created',timeout:5000});
            toaster.pop('info', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.createSuccessfully"));
            $location.path('/assetApp/list');

        },
            function(error){
            var errors = [];
            errors = error.data.errors;
            if(error.status == "404")
                $location.path("/404");
            else if(error.status == "500")
                $location.path("/500");
            else{
                for(err in errors){
                    if(errors[err]["field"] == "name")
                    toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")
                    ("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    else if(errors[err]["field"] == "email")
                        toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                   else {
                        toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")
                        ("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));
                    }
                }

            }

        });
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

    }
  }) ;


