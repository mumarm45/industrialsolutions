/**
 * Created by Umar on 11/10/14.
 */

angular.module('indapp')
    .controller('detailAssetAppController',function($rootScope,$scope,$http,crudOperationAssetApp,$q,$location,toaster,$routeParams,$filter,ngTableParams,$modal,crudOperationUser) {
        $scope.assetAppId = $routeParams['id'];
        $scope.createdByUser = {};
        $scope.updatedByUser= {};
        
        crudOperationAssetApp.show({id: $scope.assetAppId}).then(function (response) {

            $scope.assetApp = response;
            console.log($scope.assetApp.createdBy.id);
            $scope.showUser($scope.assetApp.createdBy.id,'createdBy');
            $scope.showUser($scope.assetApp.updatedBy.id,'updatedBy');

        }, function (error) {
            // $modalInstance.close();
            if (error.status == "404")
                $location.path("/404");
            else if (error.status == "500")
                $location.path("/500");
            /*else
             $location.path("/400");*/
        });

        $scope.editEntityAssetApp = function(data,assetApp,prop){
            assetApp[prop]=data;
            var d= $q.defer();
            $scope.assetAppObject = {
                id:assetApp.id,
                name:assetApp.name,
                make:assetApp.make,
                model:assetApp.model,
                expiryDate:assetApp.expiryDate,
                serialNumber:assetApp.serialNumber,
                size:assetApp.size,
                purchaseDate:assetApp.purchaseDate,
                warrantyDate:assetApp.warrantyDate,
                warrant:assetApp.warrant,
                'updatedBy':$rootScope._user.id
            };
            crudOperationAssetApp.update($scope.assetAppObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.assetApp"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };


        $scope.showUser = function (userId,val) {
            var d = $q.defer();
             crudOperationUser.show({ id: userId }).then(function (response) {
                 if(val == 'createdBy')
                 $scope.createdByUser = response;
                 else
                 $scope.updatedByUser = response;
                 d.resolve(response);

            }, function (error) {
                 d.reject(error);
            });
            return d.promise;
        };
    });
