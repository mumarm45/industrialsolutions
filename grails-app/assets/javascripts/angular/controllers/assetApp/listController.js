/**
 * Created by MUM on 10/27/2014.
 */

angular.module('indapp')
    .controller('listAssetAppController',function($timeout,toaster,$interval,$filter,$scope,
                                                  $rootScope,$modal,$http,utils,$location,ngTableParams
        ,$routeParams,crudOperationAssetApp,$q,$translate) {
        $scope.assetApps =[];
        $scope.hideShow = false;
        $scope.alerts = [];
        


        var inArray = Array.prototype.indexOf ?
            function (val, arr) {
                return arr.indexOf(val)
            } :
            function (val, arr) {
                var i = arr.length;
                while (i--) {
                    if (arr[i] === val) return i;
                }
                return -1;
            }
        $scope.isActiveStatus = function(column) {
            var blockLabel="No";
            var activeLabel="Yes";
            $translate("com.app.ind.assetApp.label.block").then(function(value){
                blockLabel = value ;
            });
            $translate("com.app.ind.assetApp.label.active").then(function(value){
                activeLabel = value ;
            });
            var def = $q.defer();
            var isActiveStatusArray = [];
            isActiveStatusArray.push({'id':'Yes',title:activeLabel});
            isActiveStatusArray.push({'id':'No',title:blockLabel});

            def.resolve(isActiveStatusArray);
            return def;
        };
        var load = function(){



            $scope.tableParams = new ngTableParams({
                page: 1,            // show first page
                count: 10           // count per page
            }, {
                total: 0, // length of data
                getData: function($defer, params) {
                    crudOperationAssetApp.getList(params.$params).then( function(response){
                            $scope.assetApps = response.assetAppsList;
                            params.total(response.assetAppsCount);





                            $defer.resolve(response.assetAppsList);

}
                        ,function(error){
                            toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.loadingError"));
                         });
                    $scope.getCheckedassetApps =function(){
                        return $filter('filter')($scope.assetApps,{checked:true});
                    };

                    $scope.allChecked=function(){
                        return $scope.getCheckedassetApps().length == $scope.assetApps.length ;
                    };
                    $scope.checkAll = function(value){
                        angular.forEach($scope.assetApps,function(bu){
                            bu.checked=value;
                        })
                    };


                    $scope.getCheckedIds = function(){
                        return _.pluck($scope.getCheckedassetApps(),'id');
                    };

                }
            });
            $scope.deletedSelected =function(){
                //console.log($scope.getCheckedIds());
                var idsArray = [];
                idsArray = $scope.getCheckedIds();

                utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                    crudOperationassetApp.selectedDelete({ids:idsArray}
                    ).then(function(response){
                            toaster.pop('danger', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.assetApp.noDeleted",{number:response.notDeletedassetApp}));
                            toaster.pop('success', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.assetApp.deleted",{number:response.deletedassetApp}));
                            $scope.tableParams.reload();
                        },
                        function(error)
                        {
                            $location.path("/404");
                            toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), error.message);

                        })

                });

            } ;


        } ;
        load();
        $scope.alerts = null;
        $scope.editAssetApp =function(id){
            $location.path('assetApp/edit/'+id);
        } ;
        $scope.detailsPage =function(id){
            $location.path('assetApp/detail/'+id);
        } ;
        $scope.editEntityAssetApp = function(data,assetApp,prop){
            assetApp[prop]=data;
            var d= $q.defer();
            $scope.assetAppObject = {
                id:assetApp.id,
                name:assetApp.name,
                make:assetApp.make,
                model:assetApp.model,
                expiryDate:assetApp.expiryDate,
                serialNumber:assetApp.serialNumber,
                size:assetApp.size,
                purchaseDate:assetApp.purchaseDate,
                warrantyDate:assetApp.warrantyDate,
                warrant:assetApp.warrant,
                'updatedBy':$rootScope._user.id
            };
            crudOperationAssetApp.update($scope.assetAppObject).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.assetApp"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.activateOrBlockAssetApp = function(data,assetApp,prop){
            assetApp[prop]=data;
            var d= $q.defer();

            crudOperationAssetApp.activeAndBlockAssetApp({id:assetApp.id,checked:assetApp.isActive}).then(function(response){
                toaster.pop('info', $filter("translate")("com.app.ind.label.assetApp"),$filter("translate")("com.app.ind.label.updateSuccessfully"));
                d.resolve();
            },function(error){
                var errors = error.data.errors
                for(err in errors){
                    d.resolve( $filter("translate")("com.app.ind.label.updateFail"));
                    toaster.pop('error', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.mustUnique",{value:errors[err]["rejected-value"],field:errors[err]["field"]}));

                }
            });

            return d.promise;
        };
        $scope.deleteAssetApp = function(index){
            var toDel = $scope.assetApps[index];
            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationAssetApp.get_api.delete(toDel,function(response){
                    $scope.assetApps.splice(index, 1);
                    toaster.pop('success',  $filter("translate")("com.app.ind.label.assetApp"),  $filter("translate")("com.app.ind.label.deleteSuccessfully"));

                },function(error){
                    toaster.pop('warning', $filter("translate")("com.app.ind.label.assetApp"),$filter("translate")("com.app.ind.label.deleteFail"));

                });
            });


        } ;
        $scope.alerttover = function(id){
           $scope.hideShow = true;

        }
        $scope.alerttleave = function(id){
            $scope.hideShow = false;
        }
        $scope.deletedSelected =function(){
            //console.log($scope.getCheckedIds());
            var idsArray = [];
            idsArray = $scope.getCheckedIds();

            utils.confirm($filter("translate")("com.app.ind.label.areYouSure")).then(function(){
                crudOperationAssetApp.selectedDelete({ids:idsArray}
                ).then(function(response){
                        toaster.pop('danger', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.assetApp.noDeleted",{number:response.notDeletedAssetApp}));
                        toaster.pop('success', $filter("translate")("com.app.ind.label.assetApp"), $filter("translate")("com.app.ind.label.assetApp.deleted",{number:response.deletedAssetApp}));
                        $scope.tableParams.reload();
                    },
                    function(error)
                    {
                        $location.path("/404");
                        toaster.pop('error', $filter("translate")("com.app.ind.label.user"), error.message);

                    })

            });

        } ;



        $scope.detailAssetApp = function(id) {
           var modalInstance = $modal.open({

                templateUrl: appBaseUrl+'/pages/assetApp/detail.html',
                controller: 'detailassetAppController',
                size:'lg',
                backdrop:'false',
                resolve: {
                    id: function(){
                        return id;
                    }
                }
            });

            modalInstance.result.then(function () {
                $scope.tableParams.reload();
            }, function () {
                $scope.tableParams.reload();
            });
        };


    });
