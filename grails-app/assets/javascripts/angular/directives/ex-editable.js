/**
 * Created by Umar on 1/1/15.
 */
angular.module('indapp.auth')
    .directive('exeditable', function(PermissionService,$compile,$rootScope,$routeParams){
        return{
            restrict: 'A',
            priority: 9999,
            terminal: true,
            link: function (scope, element, attrs) {
               var data = JSON.parse(attrs.exeditable);
               var type = data.type;
               var permission = data.permission;
               var modelValue = data.model;
                var flag = PermissionService.hasPermission(permission);
                var editableVal = "editable-"+type;
                if(flag)
                attrs.$set(editableVal, modelValue);
                else{
                    var userId = $routeParams['id'];
                    permission = permission+":"+userId;
                    flag=PermissionService.hasPermission(permission);
                    if(flag)
                        attrs.$set(editableVal, modelValue);

                }
                $compile(element,null,9999)(scope);
            }
        }
    })
    .directive('tableBorder',function($compile){
         return{
             restrict: 'A',
             priority: 0,
             terminal: true,
             scope:{
                 'changevalue':'='
             },
             link:function(scope, element, attrs){
              var checked = attrs.valchecked;
               scope.$watch('changevalue',function(nvalue,ovalue){
                    if(nvalue){
                         attrs.$set("style", "background-color: green");
                     }
                     else
                         attrs.$set("style", "background-color: blue");
                 })   ;
                 $compile(element,null,0)(scope);

             }
         }
    })
