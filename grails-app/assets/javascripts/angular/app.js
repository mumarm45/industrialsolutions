angular.module('indapp.auth', []);

var indapp = angular.module('indapp', [
        'ngRoute',
  'indapp.auth',
  'toaster',
  'ngResource',
  'ui.bootstrap',
  'xeditable',
  'bootstrap-switch',
  'ngCookies',
  'angularMoment',
  'pascalprecht.translate',
  'ngTable'])
    .constant('AUTH_EVENTS', {
        loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized',
        notFound: 'auth-not-found',
        internalError: 'auth-internal-error',
        permissionDenied: 'permission-denied',
        tokenExpired:'token-expired'


    })
.run(function(editableOptions,$http,$location) {
        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
})
    .config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
	 when('/widgets', {
        templateUrl: 'pages/widgets.html'
      }).
	  //Charts
	   when('/login', {
        templateUrl: 'pages/auth/login.html'
      }).
        //user
	   when('/user/index/', {
        templateUrl: 'pages/user/list.html'
      }).
      when('/user/create', {
            templateUrl: 'pages/user/create.html'
      }).
      when('/user/list', {
         templateUrl: 'pages/user/list.html'
      }).when('/permission/list',{
         templateUrl:'pages/permission/list.html'
        }).
        when('/user/detail/:id',{
            templateUrl:'pages/user/userDetail.html'
        })
        .when('/inline', {
        templateUrl: 'pages/charts/inline.html'
      }).
        //Role
        when('/role/index/', {
            templateUrl: 'pages/role/list.html'
        }).
        when('/role/create', {
            templateUrl: 'pages/role/create.html'
        }).
        when('/role/detail/:id', {
            templateUrl: 'pages/role/roleDetail.html'
        }).
        when('/role/list', {
            templateUrl: 'pages/role/list.html'
        })
      //investor
        .when('/investor/index/', {
          templateUrl: 'pages/investor/list.html'
      }).
          when('/investor/create', {
              templateUrl: 'pages/investor/create.html'
          }).
          when('/investor/list', {
              templateUrl: 'pages/investor/list.html'
          }).when('/investor/details/:id',{
            templateUrl: 'pages/investor/detail.html'
        })
        //assetApp
        .when('/assetApp/index/', {
            templateUrl: 'pages/assetApp/list.html'
        }).
        when('/assetApp/create', {
            templateUrl: 'pages/assetApp/create.html'
        }).
        when('/assetApp/list', {
            templateUrl: 'pages/assetApp/list.html'
        }).when('/assetApp/details/:id',{
            templateUrl: 'pages/assetApp/detail.html'
        })
	 //customer
        .when('/customer/index/', {
            templateUrl: 'pages/customer/list.html'
        }).
        when('/customer/create', {
            templateUrl: 'pages/customer/create.html'
        }).
        when('/customer/list', {
            templateUrl: 'pages/customer/list.html'
        }).when('/customer/details/:id',{
            templateUrl: 'pages/customer/detail.html'
        })
        //workOrder
        .when('/workOrder/index/', {
            templateUrl: 'pages/workOrder/list.html'
        }).
        when('/workOrder/create', {
            templateUrl: 'pages/workOrder/create.html'
        }).
        when('/workOrder/list', {
            templateUrl: 'pages/workOrder/list.html'
        }).when('/workOrder/details/:id',{
            templateUrl: 'pages/workOrder/detail.html'
        })
        //booking
        .when('/booking/index/', {
            templateUrl: 'pages/booking/list.html'
        }).
        when('/booking/create', {
            templateUrl: 'pages/booking/create.html'
        }).
        when('/booking/list', {
            templateUrl: 'pages/booking/list.html'
        }).when('/booking/details/:id',{
            templateUrl: 'pages/booking/detail.html'
        })
        //employee
        .when('/employee/index/', {
            templateUrl: 'pages/employee/list.html'
        }).
        when('/employee/create', {
            templateUrl: 'pages/employee/create.html'
        }).
        when('/employee/list', {
            templateUrl: 'pages/employee/list.html'
        }).when('/employee/details/:id',{
            templateUrl: 'pages/employee/detail.html'
        })
        //jobs
        .when('/jobs/index/', {
            templateUrl: 'pages/jobs/list.html'
        }).
        when('/jobs/create', {
            templateUrl: 'pages/jobs/create.html'
        }).
        when('/jobs/list', {
            templateUrl: 'pages/jobs/list.html'
        }).when('/jobs/details/:id',{
            templateUrl: 'pages/jobs/detail.html'
        })
	  //ERROR
        .when('/404', {
        templateUrl: 'pages/errors/404.html'
      }).
      when('/500', {
        templateUrl: 'pages/errors/500.html'
      }).
	  when('/blank', {
        templateUrl: 'pages/examples/blank.html'
      }).
	  when('/invoice', {
        templateUrl: 'pages/examples/invoice.html'
      }).
	  
      otherwise({
        redirectTo: '/index',
		templateUrl: 'pages/home.html'
      });
  }])
    .config(['$httpProvider',function($httpProvider){
        $httpProvider.interceptors.push([
            '$injector', function ($injector) {
                return $injector.get('AuthInterceptor');
            }
        ]);
    }])
    .config(['$translateProvider',function($translateProvider){

        $translateProvider.useUrlLoader('/industrial_solutions/translate');
        $translateProvider.preferredLanguage("en");

        //Remember language
        $translateProvider.useCookieStorage();
        $translateProvider.fallbackLanguage('en');
    }])



