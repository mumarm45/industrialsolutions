angular.module("indapp")
    .factory('permissionOperation',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var permissions = $resource(appBaseUrl+'/permission/',{id:'@id'},{
            get_list:{
                method:'GET',
                url: appBaseUrl+'/permission/index/',
                isArray:false
            }
        })
        return{
            'getList':  function(params){
                var defered  = $q.defer();
                permissions.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
        }

    }])