angular.module("indapp")
    .factory('crudOperationAssetApp',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var assetApps = $resource('../assetApp/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/assetApp/update/',
                params:{
                    id:'@id',
                    name:'@name',
                    make:'@make',
                    model:'@model',
                    size:'@size',
                    serialNumber:'@serialNumber',
                    warranty:'@warranty',
                    purchaseDate:'@purchaseDate',
                    expiryDate:'@expiryDate',
                    warrantyDate:'@warrantyDate',
                    'updatedBy':'@updatedBy'

                }

            },
            get_count:{
                method:'GET',
                url: '../assetApp/listCount/',
                isArray:false
            } ,
            get_list:{
                method:'GET',
                url: appBaseUrl+'/assetApp/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/assetApp/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/assetApp/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/assetApp/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/assetApp/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(assetApp){
                var defered  = $q.defer();
                assetApps.update(assetApp,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(assetApp){
                var defered  = $q.defer();
                assetApps.deletedSelected(assetApp,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                assetApps.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:assetApps,
            'getCount':  function(assetApp){
                var defered  = $q.defer();
                assetApps.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                assetApps.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

