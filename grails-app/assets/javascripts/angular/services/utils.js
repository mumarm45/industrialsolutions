/**
 * Created by Umar on 9/24/14.
 */


angular.module('indapp').factory('utils',['$modal','$rootScope',function($modal,$rootScope){

    var confirm_dialog = function(message){

      return   $modal.open({
            templateUrl: appBaseUrl+'/pages/dialog/dialog.html',
            controller: 'dialogController',
            resolve: {
                dialog_data: function () {
                    return { message:message };
                }
            }
        }).result;
    }
        return {
            confirm:confirm_dialog
        }


}]);