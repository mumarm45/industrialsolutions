angular.module("indapp")
    .factory('crudOperationCustomer',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var customers = $resource('../customer/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/customer/update/',
                params:{
                    id:'@id',
                    name:'@name',
                    position:'@position',
                    companyName:'@companyName',
                    customerType:'@customerType',
                    billingAddress:'@billingAddress',
                    reference:'@reference',
                    mobileNumberPrimary:'@mobileNumberPrimary',
                    mobileNumberSecondary:'@mobileNumberSecondary',
                    landLine:'@landLine',
                    email:'@email',
                    web:'@web',
                    remark:'@remark',
                    status:'@status',
                    deactivateReason:'@deactivateReason',
                    'updatedBy':'@updatedBy'

                }

            },
            get_count:{
                method:'GET',
                url: '../customer/listCount/',
                isArray:false
            } ,
            get_list:{
                method:'GET',
                url: appBaseUrl+'/customer/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/customer/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/customer/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/customer/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/customer/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(customer){
                var defered  = $q.defer();
                customers.update(customer,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(customer){
                var defered  = $q.defer();
                customers.deletedSelected(customer,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                customers.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:customers,
            'getCount':  function(customer){
                var defered  = $q.defer();
                customers.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                customers.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

