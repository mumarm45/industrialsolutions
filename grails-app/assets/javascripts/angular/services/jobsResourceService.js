angular.module("indapp")
    .factory('crudOperationJobs',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var jobss = $resource('../jobs/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/jobs/update/',
                params:{
                    id:'@id'
                    }

            },
            get_count:{
                method:'GET',
                url: '../jobs/listCount/'
            } ,
            findCustomer:{
                method:'GET',
                url: appBaseUrl+'/jobs/findCustomer/',
                isArray:false
            },
            getEmployeeList:{
                method:'GET',
                url: appBaseUrl+'/jobs/getEmployeeList/',
                isArray:true
            },
            get_list:{
                method:'GET',
                url: appBaseUrl+'/jobs/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/jobs/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/jobs/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/jobs/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/jobs/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(jobs){
                var defered  = $q.defer();
                jobss.update(jobs,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(jobs){
                var defered  = $q.defer();
                jobss.deletedSelected(jobs,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                jobss.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:jobss,
            'getCount':  function(jobs){
                var defered  = $q.defer();
                jobss.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                jobss.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'findCustomer':function(name){
                var defered  = $q.defer();
                jobss.findCustomer(name,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'getEmployeeList':function(){
                var defered  = $q.defer();
                jobss.getEmployeeList(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

