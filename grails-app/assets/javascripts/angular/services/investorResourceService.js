angular.module("indapp")
    .factory('crudOperationInvestor',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var investors = $resource('../investor/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/investor/update/',
                params:{
                    id:'@id',
                    name:'@name',
                    sirName:'@sirName',
                    address:'@address',
                    reference:'@reference',
                    mobileNumber:'@mobileNumber',
                    email: '@email',
                    bankName:'@bankName',
                    branchName:'@branchName',
                    branchCode:'@branchCode',
                    bankAccountType:'@bankAccountType',
                    bankAccount:'@bankAccount',
                    deActivateReason:'@deActivateReason',
                    investedAmount:'@investedAmount',
                    amountInPercentage:'@amountInPercentage',
                    status:'@status',
                    deactivateReason:'@deactivateReason',
                    'updatedBy':'@updatedBy'

                }

            },
            get_count:{
                method:'GET',
                url: '../investor/listCount/',
                isArray:false
            } ,
            get_list:{
                method:'GET',
                url: appBaseUrl+'/investor/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/investor/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/investor/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/investor/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/investor/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(investor){
                var defered  = $q.defer();
                investors.update(investor,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(investor){
                var defered  = $q.defer();
                investors.deletedSelected(investor,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                investors.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:investors,
            'getCount':  function(investor){
                var defered  = $q.defer();
                investors.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                investors.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

