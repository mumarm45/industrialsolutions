angular.module("indapp")
    .factory('crudOperationWorkOrder',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var workOrders = $resource('../workOrder/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/workOrder/update/',
                params:{
                    id:'@id',
                    bookingOrder:'@bookingOrder',
                    customerName:'@customerName',
                    customer:'@customer',
                    committedDate:'@committedDate',
                    'updatedBy':'@updatedBy'

                }

            },
            get_count:{
                method:'GET',
                url: '../workOrder/listCount/'
            } ,
            findCustomer:{
                method:'GET',
                url: appBaseUrl+'/workOrder/findCustomer/',
                isArray:false
            },
            getCustomerList:{
                method:'GET',
                url: appBaseUrl+'/workOrder/getCustomerList/',
                isArray:true
            },
            get_list:{
                method:'GET',
                url: appBaseUrl+'/workOrder/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/workOrder/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/workOrder/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/workOrder/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/workOrder/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(workOrder){
                var defered  = $q.defer();
                workOrders.update(workOrder,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(workOrder){
                var defered  = $q.defer();
                workOrders.deletedSelected(workOrder,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                workOrders.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:workOrders,
            'getCount':  function(workOrder){
                var defered  = $q.defer();
                workOrders.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                workOrders.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'findCustomer':function(name){
                var defered  = $q.defer();
                workOrders.findCustomer(name,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'getCustomerList':function(){
                var defered  = $q.defer();
                workOrders.getCustomerList(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

