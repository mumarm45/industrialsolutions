angular.module("indapp")
    .factory('crudOperationEmployee',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var employees = $resource('../employee/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/employee/update/',
                params:{
                    id:'@id',
                    name:'@name',
                    fullName:'@fullName',
                    sirName:'@sirName',
                    dateOfBirth:'@dateOfBirth',
                    address:'@address',
                    reference:'@reference',
                    idCard:'@idCard',
                    mobileNumberPrimary:'@mobileNumberPrimary',
                    mobileNumberSecondary:'@mobileNumberSecondary',
                    designation:'@designation',
                    salary:'@salary',
                    status:'@status',
                    deactivateReason:'@deactivateReason',
                    'updatedBy':'@updatedBy'

                }

            },
            get_count:{
                method:'GET',
                url: '../employee/listCount/',
                isArray:false
            } ,
            get_list:{
                method:'GET',
                url: appBaseUrl+'/employee/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/employee/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/employee/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/employee/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/employee/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(employee){
                var defered  = $q.defer();
                employees.update(employee,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(employee){
                var defered  = $q.defer();
                employees.deletedSelected(employee,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                employees.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:employees,
            'getCount':  function(employee){
                var defered  = $q.defer();
                employees.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                employees.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

