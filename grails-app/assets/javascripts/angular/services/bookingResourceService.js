angular.module("indapp")
    .factory('crudOperationBooking',['$q','$resource','$rootScope',function($q,$resource,$rootScope){
        var bookings = $resource('../booking/',{id : '@id' },  {
            update:{ method:'PUT',
                url:appBaseUrl+'/booking/update/',
                params:{
                    id:'@id',
                    bookingOrder:'@bookingOrder',
                    description:'@description',
                    customerName:'@customerName',
                    customer:'@customer',
                    committedDate:'@committedDate',
                    estimatedTime:'@estimatedTime',
                    'updatedBy':'@updatedBy'

                }

            },
            get_count:{
                method:'GET',
                url: '../booking/listCount/'
            } ,
            findCustomer:{
                method:'GET',
                url: appBaseUrl+'/booking/findCustomer/',
                isArray:false
            },
            getCustomerList:{
                method:'GET',
                url: appBaseUrl+'/booking/getCustomerList/',
                isArray:true
            },
            get_list:{
                method:'GET',
                url: appBaseUrl+'/booking/index/',
                isArray:false
            },
            show:{
                method:'GET',
                url:appBaseUrl+'/booking/show/'
                ,params:{
                    id:'@id'
                }
            },
            delete:{
                method:'DELETE',
                url:appBaseUrl+'/booking/delete/'

            },
            save:{
                method:'POST',
                url:appBaseUrl+'/booking/save'
            } ,
            deletedSelected:{
                method:'POST',
                url:appBaseUrl+'/booking/deleteSelected/'
                ,params:{
                    ids:'@ids'
                }
            }

        })
        return{
            'update' : function(booking){
                var defered  = $q.defer();
                bookings.update(booking,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'selectedDelete' : function(booking){
                var defered  = $q.defer();
                bookings.deletedSelected(booking,function(response){defered.resolve(response);},function(error){ defered.reject(error);});
                return defered.promise;
            },
            'getList':  function(params){
                var defered  = $q.defer();
                bookings.get_list(params,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
             get_api:bookings,
            'getCount':  function(booking){
                var defered  = $q.defer();
                bookings.get_count(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }
            ,
            'show':function(id){
                var defered  = $q.defer();
                bookings.show(id,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'findCustomer':function(name){
                var defered  = $q.defer();
                bookings.findCustomer(name,function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            },
            'getCustomerList':function(){
                var defered  = $q.defer();
                bookings.getCustomerList(function(response){ defered.resolve(response); },
                    function(error)
                    { defered.reject(error);});
                return defered.promise;
            }

        }
    }]);

