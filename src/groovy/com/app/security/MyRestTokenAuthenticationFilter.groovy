package com.app.security

import org.apache.shiro.web.util.WebUtils

import javax.servlet.http.HttpServletRequest

class MyRestTokenAuthenticationFilter extends RestTokenAuthenticationFilter {

    protected String getToken(request) {
       // return request.headers["X-AUTH-TOKEN"]

        HttpServletRequest httpRequest = WebUtils.toHttp(request);
     //println httpRequest.getHeader("X-AUTH-TOKEN")
       return httpRequest.getHeader("X-AUTH-TOKEN")
    }
}
